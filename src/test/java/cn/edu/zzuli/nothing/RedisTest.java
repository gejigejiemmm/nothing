package cn.edu.zzuli.nothing;

import cn.edu.zzuli.nothing.bean.Program;
import cn.edu.zzuli.nothing.bean.ProgramDetail;
import cn.edu.zzuli.nothing.bean.User;
import cn.edu.zzuli.nothing.mapper.ProgramDetailMapper;
import cn.edu.zzuli.nothing.mapper.UserMapper;
import cn.edu.zzuli.nothing.utils.BaseUtils;
import cn.edu.zzuli.nothing.utils.RedisUtil;
import cn.edu.zzuli.nothing.utils.TokenUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.cache.CacheProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;

@SpringBootTest
public class RedisTest {

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    @Autowired
    RedisUtil redisUtil;

    @Test
    void testRedis() {
        System.out.println(redisTemplate.opsForValue().get("k1"));
    }

    @Test
    void testRanking() {
        List<Object> ranking = redisUtil.lGet("ranking", 0, -1);
        for (int i = 0; i < ranking.size(); i+=2) {
            if(ranking.get(i).equals("胡一涵")) {
                if (i != 0){
                    System.out.println(i);
                    break;
                }
                System.out.println(i + 1);
                break;
            }
        }
    }

    @Autowired
    UserMapper userMapper;

    @Autowired
    ProgramDetailMapper programDetailMapper;


    @Test
    void testRefresh() {
        System.out.println(redisUtil.get("8"));
        System.out.println(redisUtil.hasKey("8"));
        String token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjdXJyZW50VGltZSI6MTU5MTY2ODIxMjYzMSwiaXNzIjoiYXV0aDAiLCJleHAiOjE1OTE2NjgyNzIsImFjY291bnQiOiI4In0.ixa1AN8G2bLdppgh3a15PmnkmDfeOSbFOHCdKxK2BmI";
        String account = TokenUtil.getAccount(token);
        System.out.println(account);
        System.out.println(redisUtil.hasKey(account));
        boolean b = redisUtil.hasKey(account);
        System.out.println(b);
        System.out.println(TokenUtil.getCurrentTime(token));

    }

    @Test
    void test() {
        String str="0123456789";
        StringBuilder sb = new StringBuilder(4);
        for(int i = 0; i < 4; i++)
        {
            char ch=str.charAt(new Random().nextInt(str.length()));
            sb.append(ch);
        }

        System.out.println(sb.toString());
    }

    @Test
    void delCode() {
        redisUtil.del("code");
    }

    @Test
    void testSigned() {
        redisUtil.getRedisTemplate().keys("user:"+"*"+":submit").forEach(System.out::println);
    }

    @Test
    void testActivity() {
        System.out.println(redisUtil.get(BaseUtils.getKey(Program.class, "isStart")));
        System.out.println(redisUtil.sGet(BaseUtils.getKey("sign:activity", "10")));
        Object o = redisUtil.get(null);
        System.out.println(o);

        Set<String> keys = redisUtil.getRedisTemplate().keys("sign:activity*");
        keys.forEach(key -> {
//            System.out.println(val);
            String pid = key.split(":")[2];
            redisUtil.sGet(key).forEach(val -> {
                ProgramDetail programDetail = new ProgramDetail();
                programDetail.setPId(Integer.parseInt(pid))
                        .setDetailsContent("activity:sign")
                        .setUId((Integer)val);
                System.out.println(pid + ":" +programDetail);

//                programDetailMapper.
//                programDetailMapper.insert();
            });
        });



    }

    @Test
    void Daily() {
        //geji
        //获取所有的用户
        List<User> users = userMapper.selectList(null);
        users.forEach(user -> {
            //根据userId来获取 user在redis中的签到记录
            List<Object> data = redisUtil.lGet(BaseUtils.getKey(User.class,
                    user.getId() + ":sign"), 0, -1);
//            System.out.println(data);
            //data不会为 null，如果没有数据则长度为0
            if (data.size() != 0 && data.size() % 2 == 0){
                List<Program> programs = new ArrayList<>();
                //说明有数据
//                addDataToList(data,programs,0,user.getId());
                System.out.println(user.getId() + ":" +data);
            }
//            redisUtil.set(BaseUtils.getKey(User.class, user.getId() + ":sign"), 0);
        });
    }

    @Test
    void getRanking() {
        redisUtil.del("ranking");

        List<User> users = userMapper.selectList(null);
        users.stream()
                //按照时间大小进行降序排序
                .sorted((user1, user2) -> {
                    Integer user1Times = (Integer) redisUtil.get(BaseUtils.getKey(User.class,
                            user1.getId()+":times"));
                    Integer user2Times = (Integer) redisUtil.get(BaseUtils.getKey(User.class,
                            user2.getId()+":times"));

                    return user2Times - user1Times;
                })
                //加入到队列
                .forEach(user -> {
                    //将id和数据，两个连在一起好了
                    redisUtil.lSet("ranking",user.getName());
                    redisUtil.lSet("ranking",redisUtil.get(BaseUtils.getKey(User.class,
                            user.getId()+":times")));
                });
    }
}
