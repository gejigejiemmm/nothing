package cn.edu.zzuli.nothing;

import cn.edu.zzuli.nothing.bean.Permission;
import cn.edu.zzuli.nothing.bean.Role;
import cn.edu.zzuli.nothing.mapper.PermissionMapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class PermissionTest {

    @Autowired
    PermissionMapper permissionMapper;

    @Test
    void testPermission() {
        List<Integer> integers = permissionMapper.permissionIdByRole(new Role().setId(2));

        LambdaQueryWrapper<Permission> permissionLambdaQueryWrapper = new LambdaQueryWrapper<>();
        permissionLambdaQueryWrapper.select(Permission::getPerssionValue)
                .in(Permission::getId, integers);

        List<Permission> permissions = permissionMapper.selectList(permissionLambdaQueryWrapper);
        permissions.forEach(System.out::println);

    }
}
