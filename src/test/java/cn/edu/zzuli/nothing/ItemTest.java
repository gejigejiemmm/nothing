package cn.edu.zzuli.nothing;

import cn.edu.zzuli.nothing.bean.ItemDetail;
import cn.edu.zzuli.nothing.bean.User;
import cn.edu.zzuli.nothing.mapper.ItemDetailMapper;
import cn.edu.zzuli.nothing.mapper.UserMapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@SpringBootTest
public class ItemTest {

    @Autowired
    UserMapper userMapper;

    @Autowired
    ItemDetailMapper itemDetailMapper;

    @Test
    void testItemDetail() {
//        LambdaQueryWrapper<ItemDetail> wrapper = new LambdaQueryWrapper<>();
//        wrapper.eq(ItemDetail::getItemId,1);
//        List<ItemDetail> itemDetails = itemDetailMapper.selectList(wrapper);
//        Map<Integer, List<ItemDetail>> detail = itemDetails.stream()
//                .collect(Collectors.groupingBy(ItemDetail::getType));
//        //获取成员详情。
//        List<ItemDetail> members = detail.get(3);
//        List<Integer> uids = members.stream().map(ItemDetail::getUid).collect(Collectors.toList());
//
//        LambdaQueryWrapper<User> userQuery = new LambdaQueryWrapper<>();
//        userQuery.select(User::getName,User::getAvater).in(User::getId,uids);
//        userMapper.selectList(userQuery).forEach(System.out::println);
    }

    @Test
    void testDelete() {
//        List<Integer> delMembers = new ArrayList<>();
//        List<String> delFileUrl = new ArrayList<>();
//        delMembers.add(1);
//        delMembers.add(8);
//        delFileUrl.add("test");
//
//        LambdaQueryWrapper<ItemDetail> itemDetail = new LambdaQueryWrapper<>();
//        itemDetail.select(ItemDetail::getId)
//                .eq(ItemDetail::getItemId,1)
//                .in(ItemDetail::getUid,delMembers);
//        List<ItemDetail> memberDetails = itemDetailMapper.selectList(itemDetail);
//        List<Integer> detailsId1 = memberDetails.stream().map(ItemDetail::getId).collect(Collectors.toList());
////        itemDetailMapper.deleteBatchIds(detailsId1);
//        System.out.println(detailsId1);
//
//        LambdaQueryWrapper<ItemDetail> fileWrapper = new LambdaQueryWrapper<>();
//        fileWrapper.select(ItemDetail::getId)
//                .eq(ItemDetail::getItemId,1)
//                .in(ItemDetail::getUrl,delFileUrl);
//        List<ItemDetail> fileDetails = itemDetailMapper.selectList(fileWrapper);
//
//        List<Integer> detailsId2 = fileDetails.stream().map(ItemDetail::getId).collect(Collectors.toList());
//        itemDetailMapper.deleteBatchIds(detailsId2);
    }
}
