package cn.edu.zzuli.nothing;


import cn.edu.zzuli.nothing.bean.User;
import cn.edu.zzuli.nothing.utils.BaseUtils;
import cn.edu.zzuli.nothing.utils.RedisUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.internet.MimeMessage;

@SpringBootTest
public class EmailTest {

    @Autowired
    JavaMailSenderImpl javaMailSender;

    @Autowired
    RedisUtil redisUtil;

    @Test
    void testSend() {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setSubject("一封来自springboot的邮件测试");
        message.setText("hello");
        message.setTo("787356076@qq.com");
        message.setFrom("787356076@qq.com");

        javaMailSender.send(message);
    }

    @Test
    void testSendHtml() {
        String html = "<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n" +
                "    <title>Document</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "    <table style=\"\n" +
                "            width: 90vw;\n" +
                "            margin: 0 auto;\n" +
                "            margin-top: 60px;\n" +
                "            z-index: 100;\n" +
                "            position: relative;\n" +
                "    \">\n" +
                "        <tr style=\"\n" +
                "                text-align: center;\n" +
                "        \">\n" +
                "            <td>\n" +
                "            </td>\n" +
                "            <td>\n" +
                "            </td>\n" +
                "\n" +
                "            <td>\n" +
                "            </td>\n" +
                "        </tr>\n" +
                "\n" +
                "        <tr style=\"\n" +
                "                text-align: center;\n" +
                "        \">\n" +
                "            <td>\n" +
                "            </td>\n" +
                "\n" +
                "            <td>\n" +
                "                <div style=\"margin:0 auto;\n" +
                "                margin-bottom: 20px;\n" +
                "                height: 150px;width: 150px;\n" +
                "                border-radius: 50%;\n" +
                "                text-align: center;\n" +
                "                background-image: linear-gradient(to top, #30cfd0 0%, #330867 100%);\n" +
                "                \">\n" +
                "        <div style=\" border-radius:50%;\n" +
                "                        margin-top: 7.5px;\n" +
                "                        display: inline-block;\n" +
                "                        width: 80%;height: 80%;\n" +
                "                        border: 8px solid white;\n" +
                "                        background-image: url('http://139.9.115.248/imgs/gif.gif');\n" +
                "                        background-size: cover;\n" +
                "                        \">\n" +
                "\n" +
                "        </div>\n" +
                "    </div>\n" +
                "            </td>\n" +
                "\n" +
                "            <td>\n" +
                "            </td>\n" +
                "        </tr>\n" +
                "\n" +
                "        <tr style=\"\n" +
                "        text-align: center;\n" +
                "\">\n" +
                "    <td>\n" +
                "    </td>\n" +
                "\n" +
                "    <td>\n" +
                "        <div style=\"\n" +
                "        width: 80vw;\n" +
                "        margin: 0px auto;\n" +
                "        text-align: center;\n" +
                "        \">\n" +
                "<p style=\"\n" +
                "        color: #69606b;\n" +
                "        font-size: 26px;\n" +
                "        margin: 0;\n" +
                "        \">图图图同学</p>\n" +
                "<p style=\"\n" +
                "        color: gray;\n" +
                "        font-size: 13px;\n" +
                "        \"\n" +
                ">邮件通知可在个人主页关闭</p>\n" +
                "</div>\n" +
                "    </td>\n" +
                "\n" +
                "    <td>\n" +
                "    </td>\n" +
                "        </tr>\n" +
                "    </table>\n" +
                "\n" +
                "    <br>\n" +
                "\n" +
                "    <table style=\"\n" +
                "    width: 90vw;\n" +
                "    margin: 0 auto;\n" +
                "    z-index: 100;\n" +
                "    position: relative;\">\n" +
                "        <tr>\n" +
                "            <td style=\"width: 50%;\">\n" +
                "                <div style=\"text-align: center;\">\n" +
                "                    <img style=\"\n" +
                "                                width: 35px;\n" +
                "                                margin: 0;\n" +
                "                    \" src=\"http://139.9.115.248/imgs/time-icon2.png\" alt=\"\">\n" +
                "                    <p style=\"\n" +
                "                            \n" +
                "                            margin: 0;\n" +
                "                            font-size: 14px;\n" +
                "                            color: #69606b;\n" +
                "                    \">本周签到总时长/h</p>\n" +
                "                    <p\n" +
                "                        style=\"\n" +
                "                            margin: 0;\n" +
                "                            color: #69606b;\n" +
                "                        \"\n" +
                "                    >3.6h</p>\n" +
                "                </div>\n" +
                "            </td>\n" +
                "            <td  style=\"width: 50%;\">\n" +
                "                <div style=\"text-align: center;\">\n" +
                "                    <img\n" +
                "                        style=\"\n" +
                "                            width: 35px;\n" +
                "                            margin: 0;\n" +
                "                        \" src=\"http://139.9.115.248/imgs/warning-icon2.png\" alt=\"\">\n" +
                "                    <p style=\"\n" +
                "                            margin: 0;\n" +
                "                            color: #69606b;\n" +
                "                            font-size: 14px;\n" +
                "                            \">本周排名</p>\n" +
                "                    <p style=\"\n" +
                "                            margin: 0;\n" +
                "                            color: #69606b;\n" +
                "                    \">1</p>\n" +
                "                </div>\n" +
                "            </td>\n" +
                "        </tr>\n" +
                "\n" +
                "        <tr  style=\"height: 135px;\">\n" +
                "            <td colspan=\"2\">\n" +
                "                <p\n" +
                "                style=\"\n" +
                "        \n" +
                "                    text-align: center;\n" +
                "                    font-size: 13px;\n" +
                "                    color: gray;\n" +
                "                \"\n" +
                "            >如有疑问，请及时联系实验室管理员</p>\n" +
                "            <p\n" +
                "            style=\"\n" +
                "        \n" +
                "            text-align: center;\n" +
                "            font-size: 13px;\n" +
                "            color: gray;\n" +
                "        \"   \n" +
                "            >\n" +
                "                &copy; 工程楼333\n" +
                "            </p></td>\n" +
                "        </tr>\n" +
                "    </table>\n" +
                "\n" +
                "\n" +
                "    <div style=\"\n" +
                "                    z-index: 100;\n" +
                "                width: 10px;\n" +
                "                height: 10px;\n" +
                "                position: absolute;\n" +
                "                left: 30vw;\n" +
                "                top: 30vh;\n" +
                "                border-radius: 50%;\n" +
                "                background-image: linear-gradient(to right, #f83600 0%, #f9d423 100%);\n" +
                "                box-shadow: 0 0 10px #f9c49a;\n" +
                "                \">\n" +
                "    </div>\n" +
                "\n" +
                "    <div style=\"\n" +
                "        z-index: 100;\n" +
                "    width: 10px;\n" +
                "    height: 10px;\n" +
                "    position: absolute;\n" +
                "    right: 10vw;\n" +
                "    top: 39vh;\n" +
                "    border-radius: 50%;\n" +
                "    background-image: linear-gradient(to top, #cfd9df 0%, #e2ebf0 100%);\n" +
                "    box-shadow: 0 0 10px #d4f3ef;\n" +
                "    \">\n" +
                "    </div>\n" +
                "\n" +
                "    <div style=\"\n" +
                "        z-index: 100;\n" +
                "    width: 10px;\n" +
                "    height: 10px;\n" +
                "    position: absolute;\n" +
                "    right: 8vw;\n" +
                "    top: 22vh;\n" +
                "    border-radius: 50%;\n" +
                "    background-image: linear-gradient(to top, #9890e3 0%, #b1f4cf 100%);\n" +
                "    box-shadow: 0 0 10px #5c2a9d;\n" +
                "    \">\n" +
                "    </div>\n" +
                "\n" +
                "    <div style=\"\n" +
                "        z-index: 100;\n" +
                "    width: 10px;\n" +
                "    height: 10px;\n" +
                "    position: absolute;\n" +
                "    left: 6vw;\n" +
                "    top: 33vh;\n" +
                "    border-radius: 50%;\n" +
                "    background-image: linear-gradient(-20deg, #b721ff 0%, #21d4fd 100%);\n" +
                "    box-shadow: 0 0 10px #035aa6;\n" +
                "    \">\n" +
                "    </div>\n" +
                "\n" +
                "    <div style=\"\n" +
                "        z-index: 100;\n" +
                "    width: 10px;\n" +
                "    height: 10px;\n" +
                "    position: absolute;\n" +
                "    left: 15vw;\n" +
                "    top: 18vh;\n" +
                "    border-radius: 50%;\n" +
                "    background-image: linear-gradient(60deg, #96deda 0%, #50c9c3 100%);\n" +
                "    box-shadow: 0 0 10px #4cd3c2;\n" +
                "    \">\n" +
                "    </div>\n" +
                "\n" +
                "    <div style=\"\n" +
                "    width: 10px;\n" +
                "    z-index: 100;\n" +
                "    height: 10px;\n" +
                "    position: absolute;\n" +
                "    left: 26vw;\n" +
                "    top: 11vh;\n" +
                "    border-radius: 50%;\n" +
                "    background-image: linear-gradient(-225deg, #22E1FF 0%, #1D8FE1 48%, #625EB1 100%);\n" +
                "    box-shadow: 0 0 10px #005082;\n" +
                "    \">\n" +
                "    </div>\n" +
                "\n" +
                "    <div style=\"\n" +
                "    z-index: 100;\n" +
                "    width: 10px;\n" +
                "    height: 10px;\n" +
                "    position: absolute;\n" +
                "    left: 50vw;\n" +
                "    top: 4vh;\n" +
                "    border-radius: 50%;\n" +
                "    background-image: linear-gradient(to right, #74ebd5 0%, #9face6 100%);\n" +
                "    box-shadow: 0 0 10px #b7efcd;\n" +
                "    \">\n" +
                "        </div>\n" +
                "\n" +
                "        <div style=\"\n" +
                "z-index: 0;\n" +
                "width: 70vw;\n" +
                "height: 70vw;\n" +
                "position: fixed;\n" +
                "left: -35vw;\n" +
                "bottom: 10px;\n" +
                "border-radius: 50%;\n" +
                "background-image: linear-gradient(-225deg, #FFFEFF 0%, #D7FFFE 100%);\n" +
                "background-image: linear-gradient(-225deg, #B7F8DB 0%, #50A7C2 100%);\n" +
                "\">\n" +
                "\n" +
                "</div>\n" +
                "\n" +
                "<div style=\"\n" +
                "  z-index: 0;\n" +
                "width: 70vw;\n" +
                "height: 70vw;\n" +
                "position: fixed;\n" +
                "right: -35vw;\n" +
                "top: 10px;\n" +
                "border-radius: 50%;\n" +
                "background-image: linear-gradient(-225deg, #FFFEFF 0%, #D7FFFE 100%);\n" +
                "background-image: linear-gradient(-225deg, #B7F8DB 0%, #50A7C2 100%);\n" +
                "\">\n" +
                "\n" +
                "</div>\n" +
                "</body>\n" +
                "</html>";
        MimeMessage message= javaMailSender.createMimeMessage();
        try {
            //true表示需要创建一个multipart message
            MimeMessageHelper helper=new MimeMessageHelper(message,true);
            helper.setFrom("787356076@qq.com");
            helper.setTo("1149858111@qq.com");
            helper.setSubject("html测试");
            helper.setText(html,true);

            javaMailSender.send(message);
            System.out.println("html格式邮件发送成功");
        }catch (Exception e){
            System.out.println("html格式邮件发送失败");
        }
    }

    @Test
    void testTime() {
//        Integer time = (Integer) redisUtil.get("user:1:times");
//        System.out.println(time);
//        Double times = time.doubleValue() / 60;
//
//        System.out.println(String.format("%.2f", times));
        System.out.println(redisUtil.lGet(BaseUtils.getKey(User.class,
                8 + ":sign"), 0, -1));
    }
}
