package cn.edu.zzuli.nothing;

import cn.edu.zzuli.nothing.bean.Program;
import cn.edu.zzuli.nothing.bean.User;
import cn.edu.zzuli.nothing.mapper.UserMapper;
import cn.edu.zzuli.nothing.service.SignService;
import cn.edu.zzuli.nothing.service.UserService;
import cn.edu.zzuli.nothing.service.aboutSign.ActivitySign;
import cn.edu.zzuli.nothing.service.aboutSign.DailySign;
import cn.edu.zzuli.nothing.service.aboutSign.Sign;
import cn.edu.zzuli.nothing.service.aboutSign.SignProxyHandler;
import cn.edu.zzuli.nothing.utils.BaseUtils;
import cn.edu.zzuli.nothing.utils.RedisUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import io.swagger.models.auth.In;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.chrono.ChronoLocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

@SpringBootTest
class NothingApplicationTests {

    @Autowired
    UserService userService;

    @Autowired
    UserMapper userMapper;

    @Autowired
    RedisUtil redisUtil;

    @Value("${wx.appid}")
    private String appid;

    @Value("${wx.appsecret}")
    private String appsecret;

    @Test
    void contextLoads() {
        //根据openid查询是否有已绑定的userid,有就时已绑定
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(User::getOpenId,"oPp6g4gtKJZSGqHW36dTQZI0h1F8");
        User user = userMapper.selectOne(queryWrapper);
        System.out.println(user);
        if (user == null) {
            user = new User().setOpenId("oPp6g4gtKJZSGqHW36dTQZI0h1F8");
            userMapper.insert(user);
        }

        System.out.println(user);
    }

    @Test
    void testUser() {
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.select(User::getName,User::getGender,User::getGradle,
                User::getBelongClass,User::getPhone,User::getEmail);

        List<User> users = userMapper.selectList(queryWrapper);
        System.out.println(users);
    }

    @Test
    void testKey() {
//        System.out.println(BaseUtils.getKey(new Program().setId(100), "hasInfo"));
//        System.out.println(BaseUtils.getKey(Program.class, "hasInfo"));

        LocalDateTime startTime = LocalDateTime.parse("2020-05-14T15:40:00");
        LocalDateTime endTime = LocalDateTime.parse("2020-05-14T15:45:00");
        //计算相差多少秒
//        Duration duration = Duration.between(startTime,endTime);
//        long millis = duration.getSeconds();
//        System.out.println(millis);

        Duration duration = Duration.between(startTime,endTime);
        System.out.println(duration.toMinutes());

//        String isStart = BaseUtils.getKey(new Program().setId(4), "isStart");
//        System.out.println(redisUtil.get(isStart));
//        System.out.println("isStart");
//        System.out.println(isStart.split(":")[1]);

    }

    @Autowired
    ActivitySign activitySign;

    @Autowired
    DailySign dailySign;

    @Autowired
    SignService signService;

    @Test
    void TestProxy() {
        SignProxyHandler proxyHandler = new SignProxyHandler();
        Sign proxy;
//        System.out.println(LocalDateTime.now());

        proxy = (Sign) proxyHandler.setTarget(dailySign);
//        boolean sign = proxy.sign(1, null);
//        System.out.println(sign);
//        System.out.println(sign);
//
//        signService.sign(10,null);


    }

    @Test
    void  testTime() {
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime parse = LocalDateTime.parse("2020-06-11 12:00:00", df);
        System.out.println(parse.toLocalTime());
        LocalTime parse1 = LocalTime.parse("10:00:00");
        Duration between = Duration.between(parse.toLocalTime(), parse1);
        System.out.println();
        System.out.println(between.getSeconds());
    }

    @Test
    void testSignResult() {
        List<Object> objects = redisUtil.lGet("user:8:sign", 0, -1);
        System.out.println(objects.size());
        System.out.println(objects);
        objects.stream().forEach(System.out::println);

        System.out.println(redisUtil.get("code"));

//        redisUtil.del("user:8:sign");
//        redisUtil.del("user:8:submit");
//        Object o = redisUtil.get("user:8:submit");
//        System.out.println(o);
//
//        List<Program> programs = new ArrayList<>();
//        addDataToList(objects,programs,0,1);
//        programs.forEach(System.out::println);
    }

    public void addDataToList(List<Object> objects, List<Program> programs, int start, int uId) {
        if (start == objects.size()) {
            return;
        }
        Program program = new Program();
        //日常签到
        program.setProgramType(1)
                .setProgramTitle("日常签到")
                .setOperatorId(uId)
                .setProgramAddtime(LocalDateTime.parse((String)objects.get(start)))
                .setProgramEndtime(LocalDateTime.parse((String)objects.get(start+1)));
        programs.add(program);
        start += 2;
        addDataToList(objects,programs,start,uId);
    }
    @Test
    void testRedisSet() {
//        redisUtil.sSet("set1","hi");
//        redisUtil.sSet("set1","hello");
//        redisUtil.sSet("set1","halo");
//        redisUtil.sSet("set1","hi");
//
//        Set<Object> set1 = redisUtil.sGet("set1");
//        set1.forEach(System.out::println);
//
//        redisUtil.set("timeTest", LocalDateTime.now().toString());
//        String timeTest = (String) redisUtil.get("timeTest");
//        LocalDateTime parse = LocalDateTime.parse(timeTest);
//        System.out.println(timeTest);
//        System.out.println(parse);
//        LocalDateTime timeTest1 = (LocalDateTime) redisUtil.get("timeTest");
//        System.out.println(timeTest1);

//        redisUtil.sSet("set1","1","2","3");
//        System.out.println(redisUtil.sGet("set1"));
//        redisUtil.lSet("list1","1");
//        redisUtil.lSet("list1","2");
//        redisUtil.lSet("list1","3");
//        System.out.println(redisUtil.lGet("list1", 0, -1));
//
//        redisUtil.lPop("list1");
//        System.out.println(redisUtil.lGet("list1", 0, -1));

//        redisUtil.set(BaseUtils.getKey(User.class, 1 + ":times"),2);
        Integer times = (Integer) redisUtil.get(BaseUtils.getKey(User.class, 1 + ":times"));
        //redis中没有Long类型，存储进去后取出来会是Interger类型。需要自行转化，不可直接强转。否则将CCE。
        System.out.println(times.longValue());
    }

    @Test
    void testCompute() {
//        redisUtil.del("ranking");
//
//        List<User> users = userMapper.selectList(null);
//        users.stream()
//                //按照时间大小进行降序排序
//                .sorted((user1, user2) -> {
//                    Integer user1Times = (Integer) redisUtil.get(BaseUtils.getKey(User.class,
//                            user1.getId()+":times"));
//                    Integer user2Times = (Integer) redisUtil.get(BaseUtils.getKey(User.class,
//                            user2.getId()+":times"));
//
//                    return user2Times - user1Times;
//                })
//                //加入到队列
//                .forEach(user -> {
//                    //将id和数据，两个连在一起好了
//                    redisUtil.lSet("ranking",user.getId());
//                    redisUtil.lSet("ranking",redisUtil.get(BaseUtils.getKey(User.class,
//                            user.getId()+":times")));
//                });

        System.out.println(redisUtil.lGet("ranking",0,-1));
    }

    @Test
    void testHasInfo() {
        System.out.println(redisUtil.get("user:8:haveInfo"));
        System.out.println(redisUtil.get(BaseUtils.getUserKey(8, "haveInfo")));
    }

}
