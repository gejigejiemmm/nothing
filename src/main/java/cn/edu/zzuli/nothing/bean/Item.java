package cn.edu.zzuli.nothing.bean;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

/**
 * <p>
 * 
 * </p>
 *
 * @author geji
 * @since 2020-05-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("oa_item")
@ApiModel(value="Item对象", description="")
public class Item implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "项目id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "项目名")
    @NotNull(message = "项目名不能为空")
    private String name;

    @ApiModelProperty(value = "成员数")
    @Min(value = 1,message = "成员数必须是大于等于1的数字")
    private Integer members;

    @ApiModelProperty(value = "文件数")
    @Min(value = 0,message = "文件数必须是大于等于0的数字")
    private Integer files;

    @ApiModelProperty(value = "github或者gitee地址")
    @NotNull(message = "url不能为空")
    private String url;

    @ApiModelProperty(value = "项目已经有多少天了")
    @Min(value = 1,message = "天数必须是大于等于1的数字")
    private Integer days;

    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:ss:mm")
    private LocalDateTime createTime;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @JsonIgnore
    private LocalDateTime updateTime;

    @TableField("is_deleted")
    @TableLogic
    @JsonIgnore
    private Boolean deleted;
    
    @TableField(exist = false)
    private List<Integer> uids;

    @TableField(exist = false)
    private List<String> fileUrls;

}
