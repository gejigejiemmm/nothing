package cn.edu.zzuli.nothing.bean.invitation;

import cn.edu.zzuli.nothing.bean.Program;
import cn.edu.zzuli.nothing.bean.ProgramInvitation;
import cn.edu.zzuli.nothing.bean.User;

//主帖的类，整个树的根
public class MainInvatation extends Invatation{

    //主帖要依赖Program类，显示主贴内容
    private Program program;

    public Program getProgram() {
        return program;
    }

    public void setProgram(Program program) {
        this.program = program;
    }

    public MainInvatation(ProgramInvitation invitation, User user) {
        super(invitation, user);
        this.program = new Program();
    }
}
