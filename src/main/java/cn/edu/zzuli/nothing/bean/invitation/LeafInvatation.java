package cn.edu.zzuli.nothing.bean.invitation;

import cn.edu.zzuli.nothing.bean.ProgramInvitation;
import cn.edu.zzuli.nothing.bean.User;

//跟帖的跟帖的类，属于整个树的叶子节点
public class LeafInvatation extends Invatation{

    public LeafInvatation(){
        super();
    }

    public LeafInvatation(ProgramInvitation invitation, User user) {
        super(invitation, user);
    }
}
