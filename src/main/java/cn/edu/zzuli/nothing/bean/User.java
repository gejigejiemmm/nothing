package cn.edu.zzuli.nothing.bean;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * <p>
 * 
 * </p>
 *
 * @author geji
 * @since 2020-05-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("oa_user")
@ApiModel(value="User对象", description="")
public class User implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    @ExcelIgnore
    private Integer id;

    @NotNull(message = "用户名不能为空")
    @NotEmpty(message = "用户名不能为空字符串")
    @ExcelProperty("姓名")
    private String name;

    @ApiModelProperty(value = "性别")
    @ExcelProperty("性别")
    private String gender;

    @NotNull(message = "密码不能为空")
    @NotEmpty(message = "不能传入空字符串")
    @ExcelIgnore
    private String password;

    @ApiModelProperty(value = "年级")
    @ExcelProperty("年级")
    private Integer gradle;

    @ApiModelProperty(value = "身份标识,1为超级管理员,2为负责人,3为普通成员")
    @ExcelProperty("年级")
    private Integer role;

    @ApiModelProperty(value = "头像url")
    @ExcelIgnore
    private String avater;

    @ApiModelProperty(value = "班级")
    @ExcelProperty("班级")
    private String belongClass;

    @Length(max = 11, min = 11, message = "传入的手机号码必须是11位")
    @ApiModelProperty(value = "手机")
    @ExcelProperty("手机号")
    private String phone;

    @Email(message = "邮箱地址非法")
    @ApiModelProperty(value = "邮箱")
    @ExcelProperty("邮箱")
    private String email;

    @ApiModelProperty(value = "微信账号唯一标识")
    @ExcelIgnore
    private String openId;

    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ExcelIgnore
    private LocalDateTime createTime;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ExcelIgnore
    private LocalDateTime updateTime;

    @TableField("is_deleted")
    @TableLogic
    @ExcelIgnore
    private Boolean deleted;


}
