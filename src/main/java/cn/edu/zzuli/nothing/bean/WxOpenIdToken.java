package cn.edu.zzuli.nothing.bean;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.shiro.authc.UsernamePasswordToken;

import java.io.Serializable;

@EqualsAndHashCode(callSuper = true)
@Data
public class WxOpenIdToken extends UsernamePasswordToken implements Serializable {
    private String openId;

    private JSONObject userInfo;

    private static final long serialVersionUID = 4812793519945855483L;
    @Override
    public Object getPrincipal() {
        return userInfo;
    }

    @Override
    public Object getCredentials() {
        return "ok";
    }

    public WxOpenIdToken(String openId, JSONObject userInfo){
        this.openId = openId;
        this.userInfo = userInfo;
    }

}
