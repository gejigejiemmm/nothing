package cn.edu.zzuli.nothing.bean;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.HashMap;
import java.util.Map;

@Data
@Accessors(chain = true)
public class Msg {

    /**
     * -0 未知
     * -100 成功
     * -200 失败
     */
    private int code;

    /**
     * 传给前端的信息
     */
    private String msg;

    /**
     * 传给前端的数据
     */
    private Map<String, Object> data = new HashMap<>();

    public static Msg success() {
        Msg result = new Msg();
        result.setCode(100);
        result.setMsg("success ヽ(ﾟ▽ﾟ)ノ");
        return result;
    }

    public static Msg fail() {
        Msg result = new Msg();
        result.setCode(200);
        result.setMsg("error ：）");
        return result;
    }

    public Msg add(String key, Object value) {
        this.getData().put(key, value);
        return this;
    }

}
