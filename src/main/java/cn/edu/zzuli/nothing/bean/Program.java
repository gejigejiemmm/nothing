package cn.edu.zzuli.nothing.bean;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author geji
 * @since 2020-05-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("oa_program")
@ApiModel(value="Program对象", description="")
public class Program implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "活动编号")
    private String programSn;

    @ApiModelProperty(value = "类型编号（1-签到，2-活动签到，3-活动。目前就这么多）")
    private Integer programType;

    @ApiModelProperty(value = "活动开启人id")
    private Integer operatorId;

    @ApiModelProperty(value = "活动标题")
    private String programTitle;

    @ApiModelProperty(value = "活动内容")
    private String programDetail;

    @ApiModelProperty(value = "活动图片")
    private String programImage;

    @ApiModelProperty(value = "活动最大人数")
    @TableField("program_maxVolume")
    private String programMaxvolume;

    @ApiModelProperty(value = "活动开始时间")
    private LocalDateTime programAddtime;

    @ApiModelProperty(value = "活动结束时间")
    private LocalDateTime programEndtime;

    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    @TableField("is_deleted")
    @TableLogic
    private Boolean deleted;

//    public static long getSerialVersionUID() {
//        return serialVersionUID;
//    }
//
//    public Integer getId() {
//        return id;
//    }
//
//    public void setId(Integer id) {
//        this.id = id;
//    }
//
//    public String getProgramSn() {
//        return programSn;
//    }
//
//    public void setProgramSn(String programSn) {
//        this.programSn = programSn;
//    }
//
//    public Integer getProgramType() {
//        return programType;
//    }
//
//    public void setProgramType(Integer programType) {
//        this.programType = programType;
//    }
//
//    public Integer getOperatorId() {
//        return operatorId;
//    }
//
//    public void setOperatorId(Integer operatorId) {
//        this.operatorId = operatorId;
//    }
//
//    public String getProgramTitle() {
//        return programTitle;
//    }
//
//    public void setProgramTitle(String programTitle) {
//        this.programTitle = programTitle;
//    }
//
//    public String getProgramDetail() {
//        return programDetail;
//    }
//
//    public void setProgramDetail(String programDetail) {
//        this.programDetail = programDetail;
//    }
//
//    public String getProgramImage() {
//        return programImage;
//    }
//
//    public void setProgramImage(String programImage) {
//        this.programImage = programImage;
//    }
//
//    public String getProgramMaxvolume() {
//        return programMaxvolume;
//    }
//
//    public void setProgramMaxvolume(String programMaxvolume) {
//        this.programMaxvolume = programMaxvolume;
//    }
//
//    public LocalDateTime getProgramAddtime() {
//        return programAddtime;
//    }
//
//    public void setProgramAddtime(LocalDateTime programAddtime) {
//        this.programAddtime = programAddtime;
//    }
//
//    public LocalDateTime getProgramEndtime() {
//        return programEndtime;
//    }
//
//    public void setProgramEndtime(LocalDateTime programEndtime) {
//        this.programEndtime = programEndtime;
//    }
//
//    public LocalDateTime getCreateTime() {
//        return createTime;
//    }
//
//    public void setCreateTime(LocalDateTime createTime) {
//        this.createTime = createTime;
//    }
//
//    public LocalDateTime getUpdateTime() {
//        return updateTime;
//    }
//
//    public void setUpdateTime(LocalDateTime updateTime) {
//        this.updateTime = updateTime;
//    }
//
//    public Boolean getDeleted() {
//        return deleted;
//    }
//
//    public void setDeleted(Boolean deleted) {
//        this.deleted = deleted;
//    }

    public Program(Integer id, String programSn, Integer programType, Integer operatorId, String programTitle, String programDetail, String programImage, String programMaxvolume, LocalDateTime programAddtime, LocalDateTime programEndtime, LocalDateTime createTime, LocalDateTime updateTime, Boolean deleted) {
        this.id = id;
        this.programSn = programSn;
        this.programType = programType;
        this.operatorId = operatorId;
        this.programTitle = programTitle;
        this.programDetail = programDetail;
        this.programImage = programImage;
        this.programMaxvolume = programMaxvolume;
        this.programAddtime = programAddtime;
        this.programEndtime = programEndtime;
        this.createTime = createTime;
        this.updateTime = updateTime;
        this.deleted = deleted;
    }

    public Program() {
        super();
    }
}
