package cn.edu.zzuli.nothing.bean;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author geji
 * @since 2020-05-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("oa_program_detail")
@ApiModel(value="ProgramDetail对象", description="")
@NoArgsConstructor
public class ProgramDetail implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "活动id")
    private Integer pId;

    @ApiModelProperty(value = "参与活动的人员id")
    private Integer uId;

    @ApiModelProperty(value = "参与活动主体内容，签到则为签到，跟帖则为帖子内容，会议暂定")
    private String detailsContent;

    @ApiModelProperty(value = "跟帖内容如果有图片的话，链接放这里")
    private String detailsImages;

    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    @TableField("is_deleted")
    @TableLogic
    private Boolean deleted;

    public ProgramDetail(Integer id, Integer pId, Integer uId, String detailsContent, String detailsImages, LocalDateTime createTime, LocalDateTime updateTime, Boolean deleted) {
        this.id = id;
        this.pId = pId;
        this.uId = uId;
        this.detailsContent = detailsContent;
        this.detailsImages = detailsImages;
        this.createTime = createTime;
        this.updateTime = updateTime;
        this.deleted = deleted;
    }
}
