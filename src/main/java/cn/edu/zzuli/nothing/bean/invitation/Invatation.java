package cn.edu.zzuli.nothing.bean.invitation;

import cn.edu.zzuli.nothing.bean.ProgramInvitation;
import cn.edu.zzuli.nothing.bean.User;

import java.util.LinkedList;
import java.util.List;

//帖子的基类，除了主题贴以外的贴都继承这个类
/*
    大致结构如下
    主贴 ---Program
        跟帖  ---ProgramInvatation
            跟帖的跟帖  ---ProgramInvatation
            跟帖的跟帖  ---ProgramInvatation
        跟帖  ---ProgramInvatation
 */
public abstract class Invatation {

    //跟帖内容，对于主贴来说，这里为null
    private ProgramInvitation invitation;
    //当前帖子的主人
    private User user;
    //帖子的跟帖（多个）
    private List<Invatation> next;

    public ProgramInvitation getInvitation() {
        return invitation;
    }

    public void setInvitation(ProgramInvitation invitation) {
        this.invitation = invitation;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Invatation> getNext() {
        return next;
    }

    public void setNext(List<Invatation> next) {
        this.next = next;
    }

    public Invatation(ProgramInvitation invitation, User user) {
        this.invitation = invitation;
        this.user = user;
        this.next = new LinkedList<Invatation>();
    }

    public void addElement(Invatation next){
        List<Invatation> list = this.getNext();
        list.add(next);
        this.setNext(list);
    }

    public void removeElement(Invatation next){
        List<Invatation> list = this.getNext();
        list.remove(next);
        this.setNext(list);
    }

    public Invatation() {
    }
}
