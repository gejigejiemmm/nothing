package cn.edu.zzuli.nothing.bean;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author geji
 * @since 2020-05-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("oa_program_invitation")
@ApiModel(value="ProgramInvitation对象", description="")
public class ProgramInvitation implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private Integer pId;

    private Integer uId;

    @ApiModelProperty(value = "前一个结点，若评论楼主贴，则为0，否则指向跟帖id")
    private Integer preNode;

    private String invitationContent;

    @ApiModelProperty(value = "点赞人数")
    private Integer invitationDing;

    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @TableLogic
    private Integer isDeleted;

    private String invitationUrl;

    public ProgramInvitation(Integer id, Integer pId, Integer uId, Integer preNode, String invitationContent, Integer invitationDing, LocalDateTime createTime, Integer isDeleted, String invitationUrl) {
        this.id = id;
        this.pId = pId;
        this.uId = uId;
        this.preNode = preNode;
        this.invitationContent = invitationContent;
        this.invitationDing = invitationDing;
        this.createTime = createTime;
        this.isDeleted = isDeleted;
        this.invitationUrl = invitationUrl;
    }
}
