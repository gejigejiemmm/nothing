package cn.edu.zzuli.nothing.task;

import cn.edu.zzuli.nothing.bean.Program;
import cn.edu.zzuli.nothing.bean.ProgramDetail;
import cn.edu.zzuli.nothing.bean.User;
import cn.edu.zzuli.nothing.mapper.ProgramDetailMapper;
import cn.edu.zzuli.nothing.mapper.ProgramMapper;
import cn.edu.zzuli.nothing.mapper.UserMapper;
import cn.edu.zzuli.nothing.utils.BaseUtils;
import cn.edu.zzuli.nothing.utils.RedisUtil;
import io.swagger.models.auth.In;
import lombok.extern.java.Log;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

@Component
@Slf4j
public class SignScheduledTask {

    @Autowired
    RedisUtil redisUtil;

    @Autowired
    UserMapper userMapper;

    @Autowired
    ProgramMapper programMapper;

    @Autowired
    ProgramDetailMapper programDetailMapper;

    //定时从redis中获取日常签到数据添加到数据库中
    @Scheduled(cron = "0 0 0 * * ? ")
    @Transactional
    public void saveDailySignFromRedis() {
        //geji
        //获取所有的用户
        List<User> users = userMapper.selectList(null);
        users.forEach(user -> {
            //根据userId来获取 user在redis中的签到记录
            List<Object> data = redisUtil.lGet(BaseUtils.getKey(User.class,
                    user.getId() + ":sign"), 0, -1);
            log.info(user.getName() + "签到数据添加,签到数据为：" + data);
            //data不会为 null，如果没有数据则长度为0
            if (data.size() != 0 && data.size() % 2 == 0){
                List<Program> programs = new ArrayList<>();
                //说明有数据
                addDataToList(data,programs,0,user.getId());

                programs.forEach(program -> {
                    programMapper.insert(program);
                });
            }
        });
    }

    //不必担心栈溢出，因为一个成员顶天了一天签个5，6次
    public void addDataToList(List<Object> objects, List<Program> programs, int start, int uId) {
        //递归条件出口
        if (start == objects.size()) {
            return;
        }
        Program program = new Program();
        //日常签到
        program.setProgramType(1)
                .setProgramTitle("日常签到")
                .setOperatorId(uId)
                .setProgramAddtime(LocalDateTime.parse((String)objects.get(start)))
                .setProgramEndtime(LocalDateTime.parse((String)objects.get(start+1)));

        //执行完毕之后，还需要清除这个redis里的数据
        redisUtil.lPop(BaseUtils.getKey(User.class,
                uId + ":sign"));
        redisUtil.lPop(BaseUtils.getKey(User.class,
                uId + ":sign"));

        programs.add(program);
        start += 2;
        //递归执行
        addDataToList(objects,programs,start,uId);
    }

    //geji
    //因为排名是时刻变化的。所以，我们这里也搞一个定时任务好了
    //一天执行两次，记得时间要在发送邮件之前，因为邮件中会清除本周的学习时间。
    @Scheduled(cron = "0 0 0/12 * * ? ")
    @Transactional
    public void computeRankings() {
        redisUtil.del("ranking");

        List<User> users = userMapper.selectList(null);
        users.stream()
                //按照时间大小进行降序排序
                .sorted((user1, user2) -> {
                    Integer user1Times = (Integer) redisUtil.get(BaseUtils.getKey(User.class,
                            user1.getId()+":times"));
                    Integer user2Times = (Integer) redisUtil.get(BaseUtils.getKey(User.class,
                            user2.getId()+":times"));

                    return user2Times - user1Times;
                })
                //加入到队列
                .forEach(user -> {
                    //将id和数据，两个连在一起好了
                    redisUtil.lSet("ranking",user.getName());
                    redisUtil.lSet("ranking",redisUtil.get(BaseUtils.getKey(User.class,
                            user.getId()+":times")));
                });
    }


    //定时获取活动签到数据添加到数据库中
    @Scheduled(cron = "0 0 0 ? * 7")
    @Transactional
    public void saveActivitySignFromRedis() {
        //首先获取活动数据
        Set<String> keys = redisUtil.getRedisTemplate().keys("sign:activity*");
        keys.forEach(key -> {
            String pid = key.split(":")[2];
            redisUtil.sGet(key).forEach(val -> {
                ProgramDetail programDetail = new ProgramDetail();
                programDetail.setPId(Integer.parseInt(pid))
                        .setDetailsContent("activity:sign")
                        .setUId((Integer)val);
//                System.out.println(pid + ":" +programDetail);

                programDetailMapper.insert(programDetail);
            });
        });

    }
}
