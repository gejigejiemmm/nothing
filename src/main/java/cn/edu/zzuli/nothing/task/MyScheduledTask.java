package cn.edu.zzuli.nothing.task;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class MyScheduledTask {

//    @Scheduled(cron = "5 0 0 * * ?")
    public void task1(){
        System.out.println("我的第一个定时任务");
    }
}
