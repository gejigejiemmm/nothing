package cn.edu.zzuli.nothing.task;

import cn.edu.zzuli.nothing.bean.User;
import cn.edu.zzuli.nothing.mapper.UserMapper;
import cn.edu.zzuli.nothing.utils.BaseUtils;
import cn.edu.zzuli.nothing.utils.RedisUtil;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailMessage;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.internet.MimeMessage;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.List;

@Component
public class EmailScheduledTask {

    @Autowired
    JavaMailSenderImpl javaMailSender;

    @Autowired
    UserMapper userMapper;

    @Autowired
    RedisUtil redisUtil;

    String name = "胡一涵";

    //签到情况
    Double signTime = 1.0;

    //本周排名
    Integer ranking = 1;

    //头像url，默认为微信头像。
    String avater = "http://139.9.115.248/imgs/gif.gif";

    //要发送的 html 本体，因为想文件搞的好看点，代码有些长，所以放在下边updateHtml 方法里了。
    String html = "";

    //每周一次
    @Scheduled(cron = "0 0 18 ? * 7")
    @Transactional
    public void sendEmail(){
        System.out.println("sendEmail...");
        //geji
        //获取需要发送的用户，还是查询所有用户
        List<User> users = userMapper.selectList(null);
        List<Object> rankings = redisUtil.lGet("ranking", 0, -1);

        users.forEach(user -> {
            //判断该用户是否开启了，邮件发送
            boolean start = (boolean) redisUtil.get(BaseUtils.getKey(User.class,
                    user.getId() + ":email"));

            if (start) {
                //邮件信息初始化
                name = user.getName();
                avater = user.getAvater();
                //既然开启了，那就开始获取该用户的签到状况
                Integer time = (Integer) redisUtil.get(BaseUtils.getKey(User.class, user.getId() + ":times"));
                //因为我们存入的是分钟
                //这里简单处理一下
                if (time != 0) {
                    signTime = time.doubleValue() / 60;
                    //保留两位小数
                    BigDecimal bg = new BigDecimal(signTime);

                    signTime = bg.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
                }else {
                    signTime = 0.0;
                }

                for (int i = 0; i < rankings.size(); i+=2) {
                    if(rankings.get(i).equals(name)) {
                        if (i != 0){
                            ranking = i;
                            break;
                        }
                        ranking = i +1;
                        break;
                    }
                }
                System.out.println(signTime);
                html = updateHtml(name, avater, ranking, signTime);

                //开始发送邮件
                MimeMessage message= javaMailSender.createMimeMessage();
                try {
                    //true表示需要创建一个multipart message
                    MimeMessageHelper helper=new MimeMessageHelper(message,true);
                    helper.setFrom("787356076@qq.com");
                    helper.setTo(user.getEmail());
                    helper.setSubject("本周学习情况");
                    helper.setText(html,true);

                    if (user.getEmail() != null) {
                        javaMailSender.send(message);
                        //还原签到的时间
                        redisUtil.set(BaseUtils.getKey(User.class, user.getId() + ":times"), 0);
                    }

                }catch (Exception e){
                    System.out.println(user.getName() + "邮件发送失败");
                }

            }
        });
    }

    public String updateHtml(String name, String avater, Integer ranking, Double signTime) {
        return "<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n" +
                "    <title>Document</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "    <table style=\"\n" +
                "            width: 90vw;\n" +
                "            margin: 0 auto;\n" +
                "            margin-top: 60px;\n" +
                "            z-index: 100;\n" +
                "            position: relative;\n" +
                "    \">\n" +
                "        <tr style=\"\n" +
                "                text-align: center;\n" +
                "        \">\n" +
                "            <td>\n" +
                "            </td>\n" +
                "            <td>\n" +
                "            </td>\n" +
                "\n" +
                "            <td>\n" +
                "            </td>\n" +
                "        </tr>\n" +
                "\n" +
                "        <tr style=\"\n" +
                "                text-align: center;\n" +
                "        \">\n" +
                "            <td>\n" +
                "            </td>\n" +
                "\n" +
                "            <td>\n" +
                "                <div style=\"margin:0 auto;\n" +
                "                margin-bottom: 20px;\n" +
                "                height: 150px;width: 150px;\n" +
                "                border-radius: 50%;\n" +
                "                text-align: center;\n" +
                "                background-image: linear-gradient(to top, #30cfd0 0%, #330867 100%);\n" +
                "                \">\n" +
                "        <div style=\" border-radius:50%;\n" +
                "                        margin-top: 7.5px;\n" +
                "                        display: inline-block;\n" +
                "                        width: 80%;height: 80%;\n" +
                "                        border: 8px solid white;\n" +
                "                        background-image: url('"+ avater +"');\n" +
                "                        background-size: cover;\n" +
                "                        \">\n" +
                "\n" +
                "        </div>\n" +
                "    </div>\n" +
                "            </td>\n" +
                "\n" +
                "            <td>\n" +
                "            </td>\n" +
                "        </tr>\n" +
                "\n" +
                "        <tr style=\"\n" +
                "        text-align: center;\n" +
                "\">\n" +
                "    <td>\n" +
                "    </td>\n" +
                "\n" +
                "    <td>\n" +
                "        <div style=\"\n" +
                "        width: 80vw;\n" +
                "        margin: 0px auto;\n" +
                "        text-align: center;\n" +
                "        \">\n" +
                "<p style=\"\n" +
                "        color: #69606b;\n" +
                "        font-size: 26px;\n" +
                "        margin: 0;\n" +
                "        \">"+ name +"</p>\n" +
                "<p style=\"\n" +
                "        color: gray;\n" +
                "        font-size: 13px;\n" +
                "        \"\n" +
                ">邮件通知可在个人主页关闭</p>\n" +
                "</div>\n" +
                "    </td>\n" +
                "\n" +
                "    <td>\n" +
                "    </td>\n" +
                "        </tr>\n" +
                "    </table>\n" +
                "\n" +
                "    <br>\n" +
                "\n" +
                "    <table style=\"\n" +
                "    width: 90vw;\n" +
                "    margin: 0 auto;\n" +
                "    z-index: 100;\n" +
                "    position: relative;\">\n" +
                "        <tr>\n" +
                "            <td style=\"width: 50%;\">\n" +
                "                <div style=\"text-align: center;\">\n" +
                "                    <img style=\"\n" +
                "                                width: 35px;\n" +
                "                                margin: 0;\n" +
                "                    \" src=\"http://139.9.115.248/imgs/time-icon2.png\" alt=\"\">\n" +
                "                    <p style=\"\n" +
                "                            \n" +
                "                            margin: 0;\n" +
                "                            font-size: 14px;\n" +
                "                            color: #69606b;\n" +
                "                    \">本周签到总时长/h</p>\n" +
                "                    <p\n" +
                "                        style=\"\n" +
                "                            margin: 0;\n" +
                "                            color: #69606b;\n" +
                "                        \"\n" +
                "                    >"+ signTime +"</p>\n" +
                "                </div>\n" +
                "            </td>\n" +
                "            <td  style=\"width: 50%;\">\n" +
                "                <div style=\"text-align: center;\">\n" +
                "                    <img\n" +
                "                        style=\"\n" +
                "                            width: 35px;\n" +
                "                            margin: 0;\n" +
                "                        \" src=\"http://139.9.115.248/imgs/warning-icon2.png\" alt=\"\">\n" +
                "                    <p style=\"\n" +
                "                            margin: 0;\n" +
                "                            color: #69606b;\n" +
                "                            font-size: 14px;\n" +
                "                            \">本周排名</p>\n" +
                "                    <p style=\"\n" +
                "                            margin: 0;\n" +
                "                            color: #69606b;\n" +
                "                    \">"+ ranking +"</p>\n" +
                "                </div>\n" +
                "            </td>\n" +
                "        </tr>\n" +
                "\n" +
                "        <tr  style=\"height: 135px;\">\n" +
                "            <td colspan=\"2\">\n" +
                "                <p\n" +
                "                style=\"\n" +
                "        \n" +
                "                    text-align: center;\n" +
                "                    font-size: 13px;\n" +
                "                    color: gray;\n" +
                "                \"\n" +
                "            >如有疑问，请及时联系实验室管理员</p>\n" +
                "            <p\n" +
                "            style=\"\n" +
                "        \n" +
                "            text-align: center;\n" +
                "            font-size: 13px;\n" +
                "            color: gray;\n" +
                "        \"   \n" +
                "            >\n" +
                "                &copy; 工程楼333\n" +
                "            </p></td>\n" +
                "        </tr>\n" +
                "    </table>\n" +
                "\n" +
                "\n" +
                "    <div style=\"\n" +
                "                    z-index: 100;\n" +
                "                width: 10px;\n" +
                "                height: 10px;\n" +
                "                position: absolute;\n" +
                "                left: 30vw;\n" +
                "                top: 30vh;\n" +
                "                border-radius: 50%;\n" +
                "                background-image: linear-gradient(to right, #f83600 0%, #f9d423 100%);\n" +
                "                box-shadow: 0 0 10px #f9c49a;\n" +
                "                \">\n" +
                "    </div>\n" +
                "\n" +
                "    <div style=\"\n" +
                "        z-index: 100;\n" +
                "    width: 10px;\n" +
                "    height: 10px;\n" +
                "    position: absolute;\n" +
                "    right: 10vw;\n" +
                "    top: 39vh;\n" +
                "    border-radius: 50%;\n" +
                "    background-image: linear-gradient(to top, #cfd9df 0%, #e2ebf0 100%);\n" +
                "    box-shadow: 0 0 10px #d4f3ef;\n" +
                "    \">\n" +
                "    </div>\n" +
                "\n" +
                "    <div style=\"\n" +
                "        z-index: 100;\n" +
                "    width: 10px;\n" +
                "    height: 10px;\n" +
                "    position: absolute;\n" +
                "    right: 8vw;\n" +
                "    top: 22vh;\n" +
                "    border-radius: 50%;\n" +
                "    background-image: linear-gradient(to top, #9890e3 0%, #b1f4cf 100%);\n" +
                "    box-shadow: 0 0 10px #5c2a9d;\n" +
                "    \">\n" +
                "    </div>\n" +
                "\n" +
                "    <div style=\"\n" +
                "        z-index: 100;\n" +
                "    width: 10px;\n" +
                "    height: 10px;\n" +
                "    position: absolute;\n" +
                "    left: 6vw;\n" +
                "    top: 33vh;\n" +
                "    border-radius: 50%;\n" +
                "    background-image: linear-gradient(-20deg, #b721ff 0%, #21d4fd 100%);\n" +
                "    box-shadow: 0 0 10px #035aa6;\n" +
                "    \">\n" +
                "    </div>\n" +
                "\n" +
                "    <div style=\"\n" +
                "        z-index: 100;\n" +
                "    width: 10px;\n" +
                "    height: 10px;\n" +
                "    position: absolute;\n" +
                "    left: 15vw;\n" +
                "    top: 18vh;\n" +
                "    border-radius: 50%;\n" +
                "    background-image: linear-gradient(60deg, #96deda 0%, #50c9c3 100%);\n" +
                "    box-shadow: 0 0 10px #4cd3c2;\n" +
                "    \">\n" +
                "    </div>\n" +
                "\n" +
                "    <div style=\"\n" +
                "    width: 10px;\n" +
                "    z-index: 100;\n" +
                "    height: 10px;\n" +
                "    position: absolute;\n" +
                "    left: 26vw;\n" +
                "    top: 11vh;\n" +
                "    border-radius: 50%;\n" +
                "    background-image: linear-gradient(-225deg, #22E1FF 0%, #1D8FE1 48%, #625EB1 100%);\n" +
                "    box-shadow: 0 0 10px #005082;\n" +
                "    \">\n" +
                "    </div>\n" +
                "\n" +
                "    <div style=\"\n" +
                "    z-index: 100;\n" +
                "    width: 10px;\n" +
                "    height: 10px;\n" +
                "    position: absolute;\n" +
                "    left: 50vw;\n" +
                "    top: 4vh;\n" +
                "    border-radius: 50%;\n" +
                "    background-image: linear-gradient(to right, #74ebd5 0%, #9face6 100%);\n" +
                "    box-shadow: 0 0 10px #b7efcd;\n" +
                "    \">\n" +
                "        </div>\n" +
                "\n" +
                "        <div style=\"\n" +
                "z-index: 0;\n" +
                "width: 70vw;\n" +
                "height: 70vw;\n" +
                "position: fixed;\n" +
                "left: -35vw;\n" +
                "bottom: 10px;\n" +
                "border-radius: 50%;\n" +
                "background-image: linear-gradient(-225deg, #FFFEFF 0%, #D7FFFE 100%);\n" +
                "background-image: linear-gradient(-225deg, #B7F8DB 0%, #50A7C2 100%);\n" +
                "\">\n" +
                "\n" +
                "</div>\n" +
                "\n" +
                "<div style=\"\n" +
                "  z-index: 0;\n" +
                "width: 70vw;\n" +
                "height: 70vw;\n" +
                "position: fixed;\n" +
                "right: -35vw;\n" +
                "top: 10px;\n" +
                "border-radius: 50%;\n" +
                "background-image: linear-gradient(-225deg, #FFFEFF 0%, #D7FFFE 100%);\n" +
                "background-image: linear-gradient(-225deg, #B7F8DB 0%, #50A7C2 100%);\n" +
                "\">\n" +
                "\n" +
                "</div>\n" +
                "</body>\n" +
                "</html>";
    }



}
