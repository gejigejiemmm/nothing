package cn.edu.zzuli.nothing.exception;

import cn.edu.zzuli.nothing.bean.Msg;
import com.auth0.jwt.exceptions.JWTDecodeException;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.UnauthorizedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * halo
 * 本来用来捕捉 shiro所报的异常，然后返会前端对应的错误信息
 */
@ControllerAdvice
@ResponseBody
public class AuthException {

    @ExceptionHandler(UnauthorizedException.class)
    public Msg handleShiroException(Exception ex) {
        return Msg.fail().setMsg("无权限");
    }

    @ExceptionHandler(AuthorizationException.class)
    public Msg AuthorizationException(Exception ex) {
        return Msg.fail().setMsg("权限认证失败");
    }

    @ExceptionHandler(AuthenticationException.class)
    public Msg AuthenticationException(Exception ex) {
        return Msg.fail().add("msg", "用户名与密码不匹配(￣_,￣ )");
    }

    @ExceptionHandler(UnknownAccountException.class)
    public Msg UnknownAccountException(Exception ex) {
        return Msg.fail().add("msg", "用户不存在，请找到合适的介绍人加入组织(๑•̀ㅂ•́)و✧");
    }

    @ExceptionHandler(IncorrectCredentialsException.class)
    public Msg IncorrectCredentialsException(Exception ex) {
        return Msg.fail().add("msg", "用户名与密码不匹配(￣_,￣ )");
    }

    @ExceptionHandler(JWTDecodeException.class)
    public Msg JWTDecodeException(Exception e) {
        return Msg.fail().add("error","token 解析失败");
    }
}
