package cn.edu.zzuli.nothing.exception;

import cn.edu.zzuli.nothing.bean.Msg;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

@ControllerAdvice
@ResponseBody
public class GlobalValidException {

    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public Msg exceptionHandler(HttpServletRequest req, MethodArgumentNotValidException e) {
//        String errmsg = e.getBindingResult().getFieldError().getField()
//                + "字段: " + e.getBindingResult().getFieldError().getDefaultMessage();
//        return errmsg;
        String errmsg = Objects.requireNonNull(e.getBindingResult().getFieldError()).getDefaultMessage();
        return Msg.fail().setMsg(errmsg);
    }
}
