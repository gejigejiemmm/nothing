package cn.edu.zzuli.nothing.exception;

import cn.edu.zzuli.nothing.bean.Msg;
import org.springframework.data.redis.RedisSystemException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
@ResponseBody
public class RedisException {
    @ExceptionHandler(RedisSystemException.class)
    public Msg RedisSystemException(Exception ex) {
        return Msg.fail().setMsg("网络好像出了点问题");
    }

}
