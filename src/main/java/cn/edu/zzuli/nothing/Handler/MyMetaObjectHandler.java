package cn.edu.zzuli.nothing.Handler;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

//自动填充 增强类 这里的cretaeTime是实体类属性而不是数据库字段名
@Slf4j
@Component
public class MyMetaObjectHandler implements MetaObjectHandler {

    //插入填充
    @Override
    public void insertFill(MetaObject metaObject) {
        //三个参数分别是 字段名，填充值，元数据
        this.setFieldValByName("createTime", LocalDateTime.now(),metaObject);
        this.setFieldValByName("updateTime", LocalDateTime.now(),metaObject);
    }

    //更新填充
    @Override
    public void updateFill(MetaObject metaObject) {
        //三个参数分别是 字段名，填充值，元数据
        this.setFieldValByName("updateTime", LocalDateTime.now(),metaObject);
    }
}
