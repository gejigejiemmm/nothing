package cn.edu.zzuli.nothing.service;

import cn.edu.zzuli.nothing.bean.ProgramDetail;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author geji
 * @since 2020-05-02
 */
public interface ProgramDetailService extends IService<ProgramDetail> {

}
