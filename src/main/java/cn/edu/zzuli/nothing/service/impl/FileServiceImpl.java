package cn.edu.zzuli.nothing.service.impl;

import cn.edu.zzuli.nothing.bean.Msg;
import cn.edu.zzuli.nothing.service.FileService;
import cn.edu.zzuli.nothing.utils.AliOssUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class FileServiceImpl implements FileService {

    @Autowired
    AliOssUtils aliOssUtils;

    @Override
    public Msg uploadFile(MultipartFile file,String folder, String name) {
        boolean res = aliOssUtils.uploadFle(file,folder, name);
        if (res) {
            return Msg.success().add("url", folder + "/" + name);
        }

        return Msg.fail().add("error", "上传失败,文件名重复或者上传服务功能有异常, 请稍后再试");
    }
}
