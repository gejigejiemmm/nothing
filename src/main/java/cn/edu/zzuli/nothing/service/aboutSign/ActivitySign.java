package cn.edu.zzuli.nothing.service.aboutSign;

import cn.edu.zzuli.nothing.bean.Msg;
import cn.edu.zzuli.nothing.utils.BaseUtils;
import cn.edu.zzuli.nothing.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 这里是活动签到
 */
@Slf4j
@Component
public class ActivitySign implements Sign {

    //不用担心一直 autowird，spring默认情况下就是单例模式
    @Autowired
    RedisUtil redisUtil;

    @Override
    public Msg sign(Integer uid, Integer pid) {
        log.info("activity sign ...");
        //geji
        //进入到此方法就证明是活动签到了
        //到现在的话，我们也有活动id了，这不就很好说了？

        //这里大可不必去来一次请求就去找数据库里添加一条数据
        //最好的方法是先存在 redis中
        redisUtil.sSet(BaseUtils.getKey("sign:activity",pid+""),uid);
//        System.out.println(BaseUtils.getKey("sign:activity",pid+""));
        return Msg.success();
    }
}
