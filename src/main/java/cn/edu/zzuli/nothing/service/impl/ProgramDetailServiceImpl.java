package cn.edu.zzuli.nothing.service.impl;

import cn.edu.zzuli.nothing.bean.ProgramDetail;
import cn.edu.zzuli.nothing.mapper.ProgramDetailMapper;
import cn.edu.zzuli.nothing.service.ProgramDetailService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author geji
 * @since 2020-05-02
 */
@Service
public class ProgramDetailServiceImpl extends ServiceImpl<ProgramDetailMapper, ProgramDetail> implements ProgramDetailService {

}
