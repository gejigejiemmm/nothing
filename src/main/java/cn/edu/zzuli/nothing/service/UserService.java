package cn.edu.zzuli.nothing.service;

import cn.edu.zzuli.nothing.bean.User;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.IService;

import java.net.URL;


/**
 * <p>
 *  服务类
 * </p>
 *
 * @author geji
 * @since 2020-05-02
 */
public interface UserService extends IService<User> {

    boolean updateByInfo(JSONObject info);

    URL userInfoToExcel();
}
