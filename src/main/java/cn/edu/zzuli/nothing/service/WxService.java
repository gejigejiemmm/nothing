package cn.edu.zzuli.nothing.service;

import cn.edu.zzuli.nothing.bean.Msg;
import cn.edu.zzuli.nothing.bean.User;

public interface WxService {

    Msg login(String code, String iv, String encryptedData);
}
