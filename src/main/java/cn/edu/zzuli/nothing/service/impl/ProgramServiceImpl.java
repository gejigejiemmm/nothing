package cn.edu.zzuli.nothing.service.impl;

import cn.edu.zzuli.nothing.bean.Program;
import cn.edu.zzuli.nothing.bean.User;
import cn.edu.zzuli.nothing.mapper.ProgramMapper;
import cn.edu.zzuli.nothing.service.ProgramService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author geji
 * @since 2020-05-02
 */
@Service
public class ProgramServiceImpl extends ServiceImpl<ProgramMapper, Program> implements ProgramService {

//    @Autowired
//    private ProgramMapper programMapper;
//
//    public void PageTest(){
//        QueryWrapper<Program> wrapper = new QueryWrapper<Program>();
//        wrapper.eq("is_deleted", 0);
//        IPage<Program> page = new IPage<>(1, 2);
//        IPage<Map<String, Object>> result = programMapper.selectMapsPage(page, wrapper);
//    }
}
