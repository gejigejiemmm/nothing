package cn.edu.zzuli.nothing.service.aboutSign;

import cn.edu.zzuli.nothing.bean.Msg;
import cn.edu.zzuli.nothing.bean.User;
import cn.edu.zzuli.nothing.utils.BaseUtils;
import cn.edu.zzuli.nothing.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.LocalDateTime;

@Component
@Slf4j
public class DailySign implements Sign {

    @Autowired
    RedisUtil redisUtil;

    @Override
    public Msg sign(Integer uid, Integer pid) {
        //至于日常签到，先理一下思路
        //因为签到和签退都是在一个接口中的，为了防止重复提交。
        //我们必须再向 redis中添加一个状态，值为签到的时间，用来表示提交的日期。
        //然后使用 aop 在执行 该方法之前，去判断一下。
        //在判断的时候，如果两个时间差为超过半小时，允许签退，否则视为第二次提交了直接过滤掉
        String start = BaseUtils.getKey("daily", uid + "");
        //签到。
        Object daily = redisUtil.get(start);
        if (daily == null) {
            log.info(start + "开始签到");
            //签到，为了防止忘记签退的情况下，给
            LocalDateTime now = LocalDateTime.now();

            //签退时间最多允许在 6小时之内完成，如果超过 3600 * 6 秒，也就是6小时，key失效，再次请求视为重新签到
            redisUtil.set(start,now.toString(),3600 * 6);
            //设置提交的时间，用来做验证
            redisUtil.set(BaseUtils.getKey(User.class,uid+":submit"),now.toString(),3600 * 6);
            return Msg.success().setMsg("签到成功!~:)");
        }

        //签退
        //获取签到时间
        String startTime = (String) redisUtil.get(start);
        String endTime = LocalDateTime.now().toString();

        Duration duration = Duration.between(LocalDateTime.parse(startTime),
                LocalDateTime.parse(endTime));
        long minutes = duration.toMinutes();

        //记录下学习的时间
        Integer redisTime = (Integer) redisUtil.get(BaseUtils.getKey(User.class, uid +":times"));
        //redis中没有Long类型，存储进去后取出来会是Integer类型。需要自行转化，不可直接强转。否则将CCE。
        long times = redisTime.longValue();
        times += minutes;
        redisUtil.set(BaseUtils.getKey(User.class,uid+":times"),times);

        //添加到用户的签到状况的集合中,在这里添加是为了，防止成员没有签退，集合中只有一个签到无签退的情况
        redisUtil.lSet(BaseUtils.getKey(User.class,uid+":sign"),startTime);
        redisUtil.lSet(BaseUtils.getKey(User.class,uid+":sign"),endTime);

        //清除在redis中的签到时间和状态,以便下一次签到
        redisUtil.del(start);
        redisUtil.del(BaseUtils.getKey(User.class,uid+":submit"));

        return Msg.success().setMsg("签退成功！~:)");
    }
}
