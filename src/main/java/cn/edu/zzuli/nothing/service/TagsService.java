package cn.edu.zzuli.nothing.service;

import cn.edu.zzuli.nothing.bean.Tags;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author geji
 * @since 2020-06-11
 */
public interface TagsService extends IService<Tags> {

}
