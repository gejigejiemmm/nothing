package cn.edu.zzuli.nothing.service.impl;

import cn.edu.zzuli.nothing.bean.Msg;
import cn.edu.zzuli.nothing.bean.Program;
import cn.edu.zzuli.nothing.bean.User;
import cn.edu.zzuli.nothing.mapper.ProgramMapper;
import cn.edu.zzuli.nothing.mapper.UserMapper;
import cn.edu.zzuli.nothing.service.SignService;
import cn.edu.zzuli.nothing.service.aboutSign.ActivitySign;
import cn.edu.zzuli.nothing.service.aboutSign.DailySign;
import cn.edu.zzuli.nothing.service.aboutSign.Sign;
import cn.edu.zzuli.nothing.service.aboutSign.SignProxyHandler;
import cn.edu.zzuli.nothing.utils.BaseUtils;
import cn.edu.zzuli.nothing.utils.RedisUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.apache.bcel.classfile.Code;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Slf4j
public class SignServiceImpl extends ServiceImpl<ProgramMapper, Program> implements SignService {

    @Autowired
    ProgramMapper programMapper;

    @Autowired
    RedisUtil redisUtil;

    @Autowired
    ActivitySign activitySign;

    @Autowired
    DailySign dailySign;

    @Autowired
    UserMapper userMapper;

    //开启活动
    @Transactional
    public boolean start(JSONObject json) {
        //geji。暂时鸽了在系统中使用Bean Validation验证参数，先把基础功能做好
        if (json.get("id") == null) {
            return false;
        }

        //关于时间
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime startTime = LocalDateTime.now();
        LocalDateTime endTime = LocalDateTime.parse(json.getString("endTime"),df);
        Program program = new Program();
        program.setOperatorId(json.getInteger("id"))
                .setProgramType(2)
                .setProgramTitle(json.getString("title"))
                .setProgramAddtime(startTime)
                .setProgramDetail(json.getString("detail"))
                .setProgramEndtime(endTime);

        //新增记录,会自动返回 id
        save(program);

        //计算相差多少秒
        Duration duration = Duration.between(startTime,endTime);
        log.info("结束时间： "+endTime);
        long millis = duration.getSeconds();
//        System.out.println(millis);

        //如果是 密码签到的话，保存状态到redis 中
        if (json.getString("code") != null) {
            redisUtil.set("code",json.getString("code"), millis);
        }

        //保存 key 到redis,value 为活动id
        redisUtil.set(BaseUtils.getKey(Program.class,"isStart"),program.getId(),millis);
        return true;
    }

    @Override
    @Transactional
    public Msg sign(Integer uid, String code) {
        //签到的时候，注意判断是不是活动签到
        String key = BaseUtils.getKey(Program.class, "isStart");
        //这个 val 即 活动id,key 为null时，val也为null，不会报错的啊。
        Integer val = (Integer) redisUtil.get(key);

        SignProxyHandler proxyHandler = new SignProxyHandler();
        Sign proxy = null;
        if ( val != null) {
            //判断 code 和 redisCode 的情况
            Object redisCode = redisUtil.get("code");
            if (redisCode != null) {
                if (code == null) {
                    return Msg.fail().setMsg("好兄弟，该签到需要code，你不给我，我怎么帮你办事！:)");
                }

                //去判断 code是否与redis中的 code 一致
                if (!code.equalsIgnoreCase(redisCode.toString())) {
                    return Msg.fail().setMsg("code 不一致，或许活动签到没有 code？？:(");
                }
            }
            //活动签到
            proxy = proxyHandler.setTarget(activitySign);
            //执行签到
            return proxy.sign(uid, val);
        } else {
            //日常签到
            proxy = proxyHandler.setTarget(dailySign);
            return proxy.sign(uid,null);
        }
    }

    @Override
    public Msg signed() {
        //活动签到的 id
        Object pid = redisUtil.get(BaseUtils.getKey(Program.class, "isStart"));
        if (pid == null) {
            //pid 为null，说明当前为日常签到，获取日常签到的人数。
            Set<String> signedKeys = redisUtil.getRedisTemplate().keys("user:" + "*" + ":submit");
            List<Integer> uids = new LinkedList<>();
            signedKeys.forEach(key -> {
                uids.add(Integer.parseInt(key.split(":")[1]));
            });
            if (uids.size() == 0) {
                return Msg.success().setMsg("暂无人签到");
            }
            //获取日常签到人数
            List<User> users = userMapper.selectList(new LambdaQueryWrapper<User>().select(User::getName, User::getAvater).in(User::getId, uids));
            return Msg.success().setMsg("成功获取日常签到人数").add("users", users);
        }
        //获取活动签到人数
        String key = BaseUtils.getKey("sign:activity", pid.toString());
        Set<Object> objects = redisUtil.sGet(key);
        List<Object> collect = objects.stream().collect(Collectors.toList());
        if (collect.size() == 0) {
            return Msg.success().setMsg("暂无人签到");
        }
        List<User> users = userMapper.selectList(new LambdaQueryWrapper<User>().select(User::getName, User::getAvater).in(User::getId, collect));
        return Msg.success().setMsg("成功获取活动签到人数:)").add("users", users);
    }

}
