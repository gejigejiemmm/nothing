package cn.edu.zzuli.nothing.service.impl;

import cn.edu.zzuli.nothing.bean.Resource;
import cn.edu.zzuli.nothing.mapper.ResourceMapper;
import cn.edu.zzuli.nothing.service.ResourceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author geji
 * @since 2020-06-11
 */
@Service
public class ResourceServiceImpl extends ServiceImpl<ResourceMapper, Resource> implements ResourceService {

}
