package cn.edu.zzuli.nothing.service.impl;

import cn.edu.zzuli.nothing.bean.Permission;
import cn.edu.zzuli.nothing.mapper.PermissionMapper;
import cn.edu.zzuli.nothing.service.PermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author geji
 * @since 2020-05-02
 */
@Service
public class PermissionServiceImpl extends ServiceImpl<PermissionMapper, Permission> implements PermissionService {

}
