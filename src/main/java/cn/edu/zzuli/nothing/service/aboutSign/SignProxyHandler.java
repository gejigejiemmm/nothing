package cn.edu.zzuli.nothing.service.aboutSign;

import cn.edu.zzuli.nothing.bean.Msg;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * sign 的调用处理程序，用来动态生成代理类
 * 2020-5-14,因为动态代理与aop混用，出现了问题，
 * 现改为 静态代理
 * @author geji
 */
public class SignProxyHandler implements Sign {
// implements InvocationHandler {

    private Sign target;

    public SignProxyHandler setTarget(Sign target) {
        this.target = target;
        return this;
    }

    @Override
    public Msg sign(Integer uid, Integer pid) {
        return target.sign(uid, pid);
    }


    //用来生成动态生成代理类
    //newProxyInstance proxy 的静态方法，用来获取一个Proxy（代理类），
    //参数分别为，（类加载器，实现的接口，InvocationHandler）
//    public Object getProxy() {
//        return  Proxy.newProxyInstance(target.getClass().getClassLoader(),
//                target.getClass().getInterfaces(),this);
//    }
//
//    //调用 target类的时候，会自动来执行 invoke方法
//    @Override
//    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
//        return method.invoke(target, args);
//    }
}
