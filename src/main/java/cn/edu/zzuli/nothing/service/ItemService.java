package cn.edu.zzuli.nothing.service;

import cn.edu.zzuli.nothing.bean.Item;
import cn.edu.zzuli.nothing.bean.Msg;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author geji
 * @since 2020-05-22
 */
public interface ItemService extends IService<Item> {

    boolean add(Item item);

    Msg get(Integer itemId);

    Msg update(JSONObject json);
}
