package cn.edu.zzuli.nothing.service;

import cn.edu.zzuli.nothing.bean.ProgramInvitation;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author geji
 * @since 2020-05-22
 */
public interface ProgramInvitationService extends IService<ProgramInvitation> {

}
