package cn.edu.zzuli.nothing.service.impl;

import cn.edu.zzuli.nothing.bean.ItemDetail;
import cn.edu.zzuli.nothing.mapper.ItemDetailMapper;
import cn.edu.zzuli.nothing.service.ItemDetailService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author geji
 * @since 2020-05-22
 */
@Service
public class ItemDetailServiceImpl extends ServiceImpl<ItemDetailMapper, ItemDetail> implements ItemDetailService {

}
