package cn.edu.zzuli.nothing.service.impl;

import cn.edu.zzuli.nothing.bean.User;
import cn.edu.zzuli.nothing.mapper.UserMapper;
import cn.edu.zzuli.nothing.service.UserService;
import cn.edu.zzuli.nothing.utils.AliOssUtils;
import cn.edu.zzuli.nothing.utils.BaseUtils;
import cn.edu.zzuli.nothing.utils.RedisUtil;
import com.alibaba.excel.EasyExcel;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.net.URL;
import java.sql.Wrapper;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author geji
 * @since 2020-05-02
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Autowired
    UserMapper userMapper;

    @Autowired
    RedisUtil redisUtil;

    @Autowired
    AliOssUtils aliOssUtils;

    @Override
    public boolean updateByInfo(JSONObject info) {
        if (info.get("id") == null
                //下边这些为了防止恶意请求来不完善信息，因为实在懒得写验证了。做一些迷惑性的操作
                || info.getString("name") == null
                || info.getString("email") == null) {
            return false;
        }
        User user = new User();
        user.setId(info.getInteger("id"))
                .setName(info.getString("name"))
                .setGradle(info.getInteger("gradle"))
                .setGender(info.getString("gender"))
//                .setPassword(info.getString("password"))
                .setBelongClass(info.getString("belongClass"))
                .setEmail(info.getString("email"))
                .setPhone(info.getString("phone"));

        userMapper.updateById(user);
        //完善信息后，修改状态
        redisUtil.set(BaseUtils.getUserKey(user.getId(),"haveInfo"),true);

        return false;
    }

    @Override
    @Transactional
    public URL userInfoToExcel() {
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.select(User::getName,User::getGender,User::getGradle,
                User::getBelongClass,User::getPhone,User::getEmail);

        List<User> users = userMapper.selectList(queryWrapper);

        String fileName = "members"+System.currentTimeMillis()+".xlsx";
        // 这里 需要指定写用哪个class去写，然后写到第一个sheet，名字为成员信息 然后文件流会自动关闭
        EasyExcel.write(fileName, User.class)
                .sheet("成员信息")
                .doWrite(users);

        //将 excel 上传到阿里云oss上
        //上传到阿里云后，返回一个url，小程序点击url可以直接下载。
        File excel = new File(fileName);
        URL url = aliOssUtils.putFile(fileName, excel);
        //在本地删除。
        excel.delete();
        return url;
    }
}
