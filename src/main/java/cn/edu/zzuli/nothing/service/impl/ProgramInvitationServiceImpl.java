package cn.edu.zzuli.nothing.service.impl;

import cn.edu.zzuli.nothing.bean.ProgramInvitation;
import cn.edu.zzuli.nothing.mapper.ProgramInvitationMapper;
import cn.edu.zzuli.nothing.service.ProgramInvitationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author geji
 * @since 2020-05-22
 */
@Service
public class ProgramInvitationServiceImpl extends ServiceImpl<ProgramInvitationMapper, ProgramInvitation> implements ProgramInvitationService {

}
