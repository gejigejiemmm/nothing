package cn.edu.zzuli.nothing.service.impl;

import cn.edu.zzuli.nothing.bean.Msg;
import cn.edu.zzuli.nothing.bean.User;
import cn.edu.zzuli.nothing.bean.WxOpenIdToken;
import cn.edu.zzuli.nothing.mapper.UserMapper;
import cn.edu.zzuli.nothing.service.WxService;
import cn.edu.zzuli.nothing.utils.AesCbcUtil;
import cn.edu.zzuli.nothing.utils.BaseUtils;
import cn.edu.zzuli.nothing.utils.RedisUtil;
import cn.edu.zzuli.nothing.utils.TokenUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.net.URI;
import java.security.Key;
import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
public class WxServiceImpl implements WxService {

    @Autowired
    UserMapper userMapper;

    @Value("${wx.appid}")
    private String appid;

    @Value("${wx.appsecret}")
    private String appsecret;

    @Autowired
    RedisUtil redisUtil;

    /**
     * 微信登录
     * @param code
     * @return
     */
    public Msg login(String code, String iv, String encryptedData) {
        //System.out.println(code);
        //geji 首先向微信发送 code 获取用户openId
        // 创建Httpclient对象
        CloseableHttpClient httpclient = HttpClients.createDefault();
        String resultString = "";
        CloseableHttpResponse response = null;
        //请求授权的地址
        String url="https://api.weixin.qq.com/sns/jscode2session?appid="+appid+"&secret="+appsecret+"&js_code="+code+"&grant_type=authorization_code";
        try {
            // 创建uri
            URIBuilder builder = new URIBuilder(url);
            URI uri = builder.build();

            // 创建http GET请求
            HttpGet httpGet = new HttpGet(uri);

            // 执行请求
            response = httpclient.execute(httpGet);
            // 判断返回状态是否为200
            if (response.getStatusLine().getStatusCode() == 200) {
                resultString = EntityUtils.toString(response.getEntity(), "UTF-8");
            }

        } catch (Exception e) {
            return Msg.fail().setMsg("code 解析不一致，可能网络有点问题。");
        }

        // 解析json
        JSONObject jsonObject = (JSONObject) JSONObject.parse(resultString);
        //代表用户唯一标识
        String openid = jsonObject.getString("openid");
//        log.info("new"openid);//oZpw443WMPZ7NBfKybhdGz206YVQ
        //对用户数据封装的的加密签名的密钥
        String session_key = jsonObject.getString("session_key");
        //log.info("new code -> session key :" + session_key);//NKtYfrke7iGyPmUH5YEDxQ==

        //code 只能使用一次，而且过期时间不保证，只要前端调用了wx.login就会刷新登录态，
        // 也就是code刷新,session_key也刷新，再进行解密会报错
        // 为了准确的获取到用户信息。需要将session key保存一下
        if (session_key == null) {
            session_key = (String) redisUtil.get(code);
        } else {
            redisUtil.set(code, session_key);
        }

        JSONObject userInfoJSON = null;
        //对 encryptedData 加密数据进行AES解密
        try {
//            log.info("iv: " + iv);
//            log.info("encryptedData: " + encryptedData);
//            log.info("session_key: " + session_key);
            String result = AesCbcUtil.decrypt(encryptedData, session_key, iv, "UTF-8");
            if (null != result && result.length() > 0) {
                //解密后的数据格式化成 json 格式
                userInfoJSON = JSONObject.parseObject(result);
//                System.out.println(userInfoJSON);
            }else {
                log.info("解密失败");
                return Msg.fail().add("error","code解密失败");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        //解密后进行相关操作
        User user = isHaveOpenIdAndTodo(userInfoJSON);
        //生成token，并返回。
        if (user == null) {
            return Msg.fail().add("error","找不到相关user");
        }

        Long currentTimeMillis = System.currentTimeMillis();
        String token = TokenUtil.sign(user.getId().toString(), currentTimeMillis);
        //System.out.println(token);
        //讲 RefreshToken 放在redis里用来进行 token续期刷新。key 为 id,string 类型...不要问为什么不用int,懒
        redisUtil.set(user.getId().toString(), currentTimeMillis, TokenUtil.REFRESH_EXPIRE_TIME);

        return Msg.success().add("token",token).add("user",user);
    }

    /**
     * 判断是不是有与 openId绑定的用户
     *  如果有判断 头像是否更新。并且返回用户数据
     *  如果没有，添加新用户，返回用户数据
     * @param userinfo
     */
    public User isHaveOpenIdAndTodo(JSONObject userinfo) {
        String openId = userinfo.getString("openId");

//        log.info("userinfo: "+openId);
        if (openId == null) {
            return null;
        }

        //根据openid查询是否有已绑定的userid,有就时已绑定
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(User::getOpenId, openId);
        User user = userMapper.selectOne(queryWrapper);

        //说明，还没有使用过系统，新增加一条数据，与 openId进行绑定
        if (user == null) {
            user = new User().setOpenId(openId)
                    .setAvater(userinfo.getString("avatarUrl"));
            userMapper.insert(user);
            redisUtil.set(BaseUtils.getUserKey(user.getId(),"haveInfo"), false);
            redisUtil.set(BaseUtils.getKey(User.class, user.getId() + ":times"), 0);
            redisUtil.set(BaseUtils.getKey(User.class, user.getId() + ":email"), true);
            user.setOpenId("");
            user.setPassword("");
            return  user;
        }

        //为了防止用户修改头像，每次判断一下。如果不一样要去修改。
        String avatarUrl = userinfo.getString("avatarUrl");
        Integer uid = user.getId();
        if (!avatarUrl.equals(user.getAvater())) {
            new Thread(() -> {
                User update = new User();
                update.setAvater(avatarUrl)
                        .setId(uid);
                //只修改头像
                userMapper.updateById(update);
            }).start();
        }
        user.setAvater(avatarUrl);
        user.setOpenId("");
        user.setPassword("");
        return user;
    }
}
