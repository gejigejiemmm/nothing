package cn.edu.zzuli.nothing.service.impl;

import cn.edu.zzuli.nothing.bean.Role;
import cn.edu.zzuli.nothing.mapper.RoleMapper;
import cn.edu.zzuli.nothing.service.RoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author geji
 * @since 2020-05-02
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements RoleService {

}
