package cn.edu.zzuli.nothing.service;

import cn.edu.zzuli.nothing.bean.Msg;
import cn.edu.zzuli.nothing.bean.Program;
import com.alibaba.fastjson.JSONObject;

public interface SignService {

    boolean start(JSONObject json);

    Msg sign(Integer uid, String code);

    Msg signed();
}
