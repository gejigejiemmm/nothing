package cn.edu.zzuli.nothing.service.impl;

import cn.edu.zzuli.nothing.bean.Tags;
import cn.edu.zzuli.nothing.mapper.TagsMapper;
import cn.edu.zzuli.nothing.service.TagsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author geji
 * @since 2020-06-11
 */
@Service
public class TagsServiceImpl extends ServiceImpl<TagsMapper, Tags> implements TagsService {

}
