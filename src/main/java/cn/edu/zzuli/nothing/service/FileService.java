package cn.edu.zzuli.nothing.service;

import cn.edu.zzuli.nothing.bean.Msg;
import org.springframework.web.multipart.MultipartFile;

public interface FileService {
    Msg uploadFile(MultipartFile file, String folder, String name);
}
