package cn.edu.zzuli.nothing.service.impl;

import cn.edu.zzuli.nothing.bean.Item;
import cn.edu.zzuli.nothing.bean.ItemDetail;
import cn.edu.zzuli.nothing.bean.Msg;
import cn.edu.zzuli.nothing.bean.User;
import cn.edu.zzuli.nothing.mapper.ItemDetailMapper;
import cn.edu.zzuli.nothing.mapper.ItemMapper;
import cn.edu.zzuli.nothing.mapper.UserMapper;
import cn.edu.zzuli.nothing.service.ItemService;
import cn.edu.zzuli.nothing.utils.AliOssUtils;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Valid;
import java.net.URL;
import java.security.Key;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author geji
 * @since 2020-05-22
 */
@Service
@Slf4j
public class ItemServiceImpl extends ServiceImpl<ItemMapper, Item> implements ItemService {

    @Autowired
    ItemMapper itemMapper;

    @Autowired
    ItemDetailMapper itemDetailMapper;

    @Autowired
    UserMapper userMapper;

    @Autowired
    AliOssUtils aliOssUtils;

    @Override
    @Transactional
    public boolean add(Item item) {
        //System.out.println(item);
        if (!save(item)){
            return false;
        }
        addDetail(item);
        return true;
    }

    @Override
    public Msg get(Integer itemId) {
        Msg res = Msg.success();

        if (itemId == 0) {
            return res.add("items",itemMapper.selectList(null));
        }
        //获取项目信息
        Item item = itemMapper.selectById(itemId);
        res.add("item",item);

        //获取详情信息
        LambdaQueryWrapper<ItemDetail> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(ItemDetail::getItemId,itemId);
        List<ItemDetail> itemDetails = itemDetailMapper.selectList(wrapper);

        //根据类别分组，1是log，2是文件，3是成员
        Map<Integer, List<ItemDetail>> detail = itemDetails.stream()
                .collect(Collectors.groupingBy(ItemDetail::getType));

        //获取成员详情。
        List<ItemDetail> members = detail.get(3);
        List<Integer> uids = members.stream().map(ItemDetail::getUid).collect(Collectors.toList());

        //根据uid获取成员的name和头像
        FutureTask<List<User>> getUser = new FutureTask<>(()-> {
            LambdaQueryWrapper<User> userQuery = new LambdaQueryWrapper<>();
            userQuery.select(User::getName,User::getAvater,User::getId).in(User::getId,uids);
            return userMapper.selectList(userQuery);
        });
        new Thread(getUser).start();

        detail.forEach((key,val) -> {
            if (key == 1) {
                res.add("logs",val);
            }else if (key == 2) {
                Map<String, URL> map = new HashMap<>();
                val.forEach(urlDetail -> {
                    map.put(urlDetail.getUrl(), aliOssUtils.getUrl(urlDetail.getUrl()));
                });
                res.add("files",map);
            }else if (key == 3) {
                try {
                    res.add("memebers",getUser.get());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            }
        });

        return res;
    }

    @Override
    @Transactional
    public Msg update(JSONObject json) {
        Integer id = json.getInteger("id");
        if (id == null) {
            return Msg.fail().setMsg("id 不能为null");
        }

        Item item = new Item();
        item.setId(id)
                .setName(json.getString("name"))
                .setMembers(json.getInteger("members"))
                //太恶心了这代码，我是不想改了。
                .setFiles(json.getInteger("files"))
                .setUrl(json.getString("url"))
                .setUids((List<Integer>) json.get("uids"))
                .setFileUrls((List<String>) json.get("fileUrls"));

        addDetail(item);

        if (json.get("log") != null) {
            String log = json.getString("log");
            ItemDetail itemDetail = new ItemDetail();
            itemDetail.setType(1)
                    .setLog(log)
                    .setItemId(item.getId());

            itemDetailMapper.insert(itemDetail);
        }
        itemMapper.updateById(item);

        List<Integer> delMembers = (List<Integer>) json.get("delmembers");
        List<String> delFileUrl = (List<String>) json.get("delfileurls");
        if ((delMembers == null || delMembers.size() == 0) &&
                ( delFileUrl == null || delFileUrl.size() == 0)) {
            return Msg.success();
        }

        //执行到这里就可以去删除详情了。删除log先不搞
        try {
            delDetail(delMembers,delFileUrl,id);
        } catch (Exception e) {
            log.error("del 操作有异常");
            return Msg.fail().setMsg("del 操作有异常");
        }

        return Msg.success();
    }

    public void addDetail(Item item) {
        //因为item的成员至少为1，所以必须要添加对应的详情记录
        List<Integer> uids = item.getUids();
        if (uids != null) {
            new Thread(() -> {
            ItemDetail itemDetail = new ItemDetail();

            uids.forEach(uid -> {
                itemDetail.setType(3)
                        .setUid(uid)
                        .setItemId(item.getId());

                itemDetailMapper.insert(itemDetail);
            });
            }).start();
        }

        //如果创建项目的时候上传了文件，前端会将文件url发送回来。
        //因为采取的是sts形式来进行直传的，所以不经由后端上传到阿里云oss。所以这个url可能仅仅是个文件名。
        if (item.getFileUrls() != null) {
            new Thread(() -> {
                ItemDetail itemDetail = new ItemDetail();

                item.getFileUrls().forEach(fileUrl -> {
                    itemDetail.setType(2)
                            .setUrl(fileUrl)
                            .setItemId(item.getId());

                    itemDetailMapper.insert(itemDetail);
                });
            }).start();
        }
    }

    public void delDetail(List<Integer> delMembers,List<String> delFileUrl,Integer itemId) throws ExecutionException, InterruptedException {
        //创建线程池
        ExecutorService service = Executors.newFixedThreadPool(2);
        Future<Boolean> memebers = null;
        Future<Boolean> files = null;

        if (delMembers != null) {
            //删除成员详情
            memebers = service.submit(() -> {
                LambdaQueryWrapper<ItemDetail> itemDetail = new LambdaQueryWrapper<>();
                itemDetail.select(ItemDetail::getId)
                        .eq(ItemDetail::getItemId,itemId)
                        .in(ItemDetail::getUid,delMembers);
                List<ItemDetail> memberDetails = itemDetailMapper.selectList(itemDetail);
                //提取id
                List<Integer> detailsId1 = memberDetails.stream().map(ItemDetail::getId).collect(Collectors.toList());
                itemDetailMapper.deleteBatchIds(detailsId1);
                return true;
            });
        }

        if (delFileUrl != null) {
            files = service.submit(() -> {
                //删除文件详情
                LambdaQueryWrapper<ItemDetail> fileWrapper = new LambdaQueryWrapper<>();
                fileWrapper.select(ItemDetail::getId)
                        .eq(ItemDetail::getItemId,itemId)
                        .in(ItemDetail::getUrl,delFileUrl);
                List<ItemDetail> fileDetails = itemDetailMapper.selectList(fileWrapper);

                List<Integer> detailsId2 = fileDetails.stream().map(ItemDetail::getId).collect(Collectors.toList());
                itemDetailMapper.deleteBatchIds(detailsId2);
                return true;
            });

        }
        if (memebers != null)
            memebers.get();
        if (files != null)
            files.get();

        service.shutdown();
    }
}
