package cn.edu.zzuli.nothing.service.aboutSign;

import cn.edu.zzuli.nothing.bean.Msg;

/**
 * 被代理的接口
 */
public interface Sign {

    Msg sign(Integer uid, Integer pid);

}