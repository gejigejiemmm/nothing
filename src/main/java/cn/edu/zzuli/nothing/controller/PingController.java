package cn.edu.zzuli.nothing.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PingController {

    //仅仅用来测试后端接口能不能用
    @GetMapping("/ping")
    public String ping() {
        return "pong!";
    }

}
