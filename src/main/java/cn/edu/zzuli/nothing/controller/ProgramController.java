package cn.edu.zzuli.nothing.controller;


import cn.edu.zzuli.nothing.bean.Msg;
import cn.edu.zzuli.nothing.bean.Program;
import cn.edu.zzuli.nothing.bean.ProgramDetail;
import cn.edu.zzuli.nothing.bean.User;
import cn.edu.zzuli.nothing.service.ProgramDetailService;
import cn.edu.zzuli.nothing.service.ProgramService;
import cn.edu.zzuli.nothing.service.impl.ProgramServiceImpl;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.security.auth.Subject;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author geji
 * @since 2020-05-02
 */
@RestController
@RequestMapping("/program")
public class ProgramController {

    @Autowired
    private ProgramService programService;

    @Autowired
    private ProgramDetailService programDetailService;

    //时间的格式化串
    private DateTimeFormatter dataformate = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    //查询当前所有活动
    @GetMapping("programeIndex")
    @ApiOperation("查询当前所有活动")
    public Msg programeIndex(){
        QueryWrapper<Program> wrapper = new QueryWrapper<Program>();
        wrapper.eq("is_deleted", 0);
        List<Program> result = programService.list(wrapper);
        if(result!=null){
            return Msg.success().add("data", result);
        }
        else{
            return Msg.fail().add("error", "网络异常，请稍后重试！");
        }
    }

    //条件检索
    @GetMapping("selectProgram")
    @ApiOperation("按照给定条件查看活动")
    public Msg selectProgram(@RequestParam(value = "id", required = false) Integer id,
                             @RequestParam(value = "ptype", required = false) Integer type,
                             @RequestParam(value = "uid", required = false) Integer uid){
        QueryWrapper<Program> wrapper = new QueryWrapper<Program>();
        if(id!=null){
            wrapper.eq("id", id);
        }
        if(type!=null){
            wrapper.eq("program_type", type);
        }
        if(uid!=null){
            wrapper.eq("operator_id", uid);
        }
        wrapper.eq("is_deleted", 0);
        List<Program> result = programService.list(wrapper);
        if(result!=null){
            return Msg.success().add("data", result);
        }
        else{
            return Msg.fail().add("error", "网络异常，请稍后重试！");
        }
    }

    @PostMapping("updateProgram")
    @ApiOperation("按照给定条件修改活动内容")
    public Msg updateProgram(@RequestBody JSONObject params){
        Integer id = params.getInteger("id");
        Program program = programService.getById(id);
        Integer uid = params.getInteger("uid");
        if(uid!=null){
            program.setOperatorId(uid);
        }
        String title = params.getString("title");
        if(title!=null){
            program.setProgramTitle(title);
        }
        String detail = params.getString("detail");
        if(detail!=null){
            program.setProgramDetail(detail);
        }
        String imageUrl = params.getString("imageUrl");
        if(imageUrl!=null){
            program.setProgramImage(imageUrl);
        }
        Integer maxVolume = params.getInteger("maxVolume");
        if(maxVolume!=null){
            program.setProgramMaxvolume(maxVolume.toString());
        }
        String StartTime = params.getString("startTime");
        if(StartTime!=null){
            LocalDateTime startTime = LocalDateTime.parse(StartTime, dataformate);
            program.setProgramAddtime(startTime);
        }
        String EndTime = params.getString("endTime");
        if(EndTime!=null){
            LocalDateTime endTime = LocalDateTime.parse(EndTime, dataformate);
            program.setProgramEndtime(endTime);
        }
        boolean result = programService.updateById(program);
        if(result == true){
            return Msg.success().add("msg", "已成功修改活动！");
        }
        else{
            return Msg.fail().add("error", "网络异常，请稍后重试！");
        }
    }

    //新建保存一个活动
    @PostMapping("saveRegister")
    @ApiOperation("新建保存一个活动")
    public Msg saveRegister(@RequestBody JSONObject params) {
        Integer id = params.getInteger("uid");
        String title = params.getString("title");
        String detail = params.getString("detail");
        String imageUrl = params.getString("imageUrl");
        Integer maxVolume = params.getInteger("maxVolume");
        String StartTime = params.getString("startTime");
        String EndTime = params.getString("endTime");
        LocalDateTime startTime = LocalDateTime.parse(StartTime, dataformate);
        LocalDateTime endTime = LocalDateTime.parse(EndTime, dataformate);
        Integer type = params.getInteger("type");
        boolean result = true;
        if ((type == 1) || (type == 2) || (type == 3)) {
            //不同活动保存方式相同
            Program program = new Program(9, "xxx", 1, id, title, detail, imageUrl, maxVolume.toString(), startTime, endTime, LocalDateTime.now(), LocalDateTime.now(), false);
            result = programService.save(program);
        } else {
            return Msg.fail().add("error", "参数错误(未设置活动类型)");
        }
        if (result == true) {
            return Msg.success().add("msg", "已成功创建活动！");
        } else {
            return Msg.fail().add("error", "网络异常，请稍后重试！");
        }
    }

}

