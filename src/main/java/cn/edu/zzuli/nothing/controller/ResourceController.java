package cn.edu.zzuli.nothing.controller;


import cn.edu.zzuli.nothing.bean.Msg;
import cn.edu.zzuli.nothing.bean.Resource;
import cn.edu.zzuli.nothing.service.ResourceService;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author geji
 * @since 2020-06-11
 */
@RestController
@RequestMapping("/resource")
public class ResourceController {

    @Autowired
    ResourceService resourceService;

    @PostMapping("/go")
    public Msg add(@RequestBody  JSONObject req) {
        String content = req.getString("content");
        Integer tagId = req.getInteger("tagId");
        if (content == null){//if (content == null || tagId == null) { 小程序提交前先不搞。
            return Msg.fail().add("error", "tag或内容不能为空");
        }
        Resource md = new Resource().setContent(content).setTagId(tagId);
        resourceService.save(md);
        return Msg.success().add("resource",md);
    }

    @GetMapping("/go/{id}")
    public Msg getOne(@PathVariable Integer id) {
        return Msg.success().add("resource",resourceService.getById(id));
    }

    @GetMapping("/go")
    public Msg getList() {
        List<Resource> resources = resourceService.list(
                new LambdaQueryWrapper<Resource>()
                .select(Resource::getId,Resource::getTitle));
        return Msg.success().add("resources",resources);
    }

}

