package cn.edu.zzuli.nothing.controller;

import cn.edu.zzuli.nothing.bean.Msg;


import cn.edu.zzuli.nothing.bean.Program;
import cn.edu.zzuli.nothing.bean.User;
import cn.edu.zzuli.nothing.mapper.UserMapper;
import cn.edu.zzuli.nothing.service.SignService;
import cn.edu.zzuli.nothing.utils.BaseUtils;
import cn.edu.zzuli.nothing.utils.RedisUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.bouncycastle.asn1.x500.style.RFC4519Style.uid;

@RestController
@RequestMapping("/sign")
public class SignController {

    @Autowired
    SignService signService;

    @Autowired
    RedisUtil redisUtil;

    @PostMapping("/activity")
    @RequiresRoles("principal")
    @ApiOperation("管理员开启活动签到")
    public Msg start(@RequestBody JSONObject json) {
        //geji首先分析一下思路
        //因为签到是放在 活动表中的。所以开启活动签到的时候，毫无疑问要去新加一条记录
        //新的记录作为 key 放在 redis中，活动时间即过期时间
        //当 成员发送签到请求的时候 首先去redis中判断这个 key是否存在，存在即活动签到，不存在则为日常签到
        if (signService.start(json)) {
            return Msg.success().add("code", json.getString("code"));
        }

        return Msg.fail();
    }

    @PostMapping("/in")
    @ApiOperation("{\"id\":\"xxx\",\"code\":\"xxx\"}签到,日常签到，签退和活动签到都是这个接口，逻辑后端有处理,如需要使用 密码签到，传入 code")
    public Msg singIn(@RequestBody JSONObject json) {
//        System.out.println(json.getString("code"));
//        System.out.println(json.getInteger("id"));

        if (json.getInteger("id") == null){
            return Msg.fail();
        }

        return signService.sign(json.getInteger("id"), json.getString("code"));
    }

    @GetMapping("/hascode")
    @ApiOperation("前端如果需要知道是不是活动签到或者活动签到有没有code，需要用这个接口")
    public Msg hasCode() {
        return Msg.success().add("code", redisUtil.get("code"));
    }

    @GetMapping("/signed")
    @ApiOperation("当前活动已经有那些成员签到了，展示用")
    public Msg signed() {
        return signService.signed();
    }

}
