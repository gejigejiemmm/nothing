package cn.edu.zzuli.nothing.controller;


import cn.edu.zzuli.nothing.bean.*;
import cn.edu.zzuli.nothing.bean.invitation.Invatation;
import cn.edu.zzuli.nothing.bean.invitation.LeafInvatation;
import cn.edu.zzuli.nothing.bean.invitation.MainInvatation;
import cn.edu.zzuli.nothing.bean.invitation.StepInvatation;
import cn.edu.zzuli.nothing.service.ProgramInvitationService;
import cn.edu.zzuli.nothing.service.ProgramService;
import cn.edu.zzuli.nothing.service.UserService;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author geji
 * @since 2020-05-22
 */
@RestController
@RequestMapping("/program-invitation")
public class ProgramInvitationController {

    @Autowired
    private ProgramInvitationService invitationService;

    @Autowired
    private ProgramService programService;

    @Autowired
    private UserService userService;

    //帖子的顶踩,跟帖（评论），区别在于内容，点赞的内容为"顶"，
    @PostMapping("ding")
    @ApiOperation("帖子的顶踩,跟帖（评论），区别在于内容，点赞的内容为ding，评论则为评论内容")
    public Msg ding(@RequestBody JSONObject params){
        Integer p_id = params.getInteger("pid");
        Integer u_id = params.getInteger("uid");
        Integer prenode = params.getInteger("prenode");
        String content = params.getString("content");
        String image = params.getString("image");
        if(content==null||content.equals("")){
            return Msg.fail().add("error", "主体内容不能为空");
        }
        ProgramInvitation invitation = new ProgramInvitation(9, p_id, u_id, prenode, content, 0, LocalDateTime.now(), 0, image);
        boolean result = invitationService.save(invitation);
        if(result == true){
            return Msg.success().add("msg", "已成功跟帖！");
        }
        else{
            return Msg.fail().add("error", "网络异常，请稍后重试！");
        }
    }

    @PostMapping("deleteInvitation")
    @ApiOperation("按照id删除指定的跟帖")
    public Msg deleteInvitation(@RequestBody JSONObject params){
        Integer id = params.getInteger("id");
        boolean result = invitationService.removeById(id);
        if(result == true){
            return Msg.success().add("msg", "已成功删除！");
        }
        else{
            return Msg.fail().add("error", "网络异常，请稍后重试！");
        }
    }


    @GetMapping("getComment")
    @ApiOperation("获取指定id的主贴的跟帖，返回一个树形结构，供前端解析")
    public Msg getComment(@RequestParam("pid") Integer p_id){
        if(p_id!=null){
            //对于主贴的跟帖，他们的pre_node为-1
            QueryWrapper<ProgramInvitation> wrapper = new QueryWrapper<ProgramInvitation>();
            wrapper.eq("p_id", p_id);
            wrapper.eq("pre_node", -1);
            wrapper.eq("is_deleted", 0);
            wrapper.ne("invitation_content", "ding"); //去掉点赞
            List<ProgramInvitation> Stepresult = invitationService.list(wrapper);
            //叶子节点
            //对于跟帖的跟帖，他们的pre_node为跟帖的id,这里获取到所有叶子节点
            QueryWrapper<ProgramInvitation> wrapper1 = new QueryWrapper<ProgramInvitation>();
            wrapper1.eq("p_id", p_id);
            wrapper1.ne("pre_node", -1);
            wrapper1.eq("is_deleted", 0);
            List<ProgramInvitation> Leafresult = invitationService.list(wrapper1);

            Program mainProgram = programService.getById(20);
            //使用组合模式组装树状结构
            List<Invatation> stepList = new LinkedList<Invatation>();
            for(ProgramInvitation programInvitation:Stepresult){
                StepInvatation one = new StepInvatation();
                one.setInvitation(programInvitation);
                String oneName = userService.getById(programInvitation.getUId()).getName();
                //只要名字
                User oneUser = new User();
                oneUser.setName(oneName);
                one.setUser(oneUser);
                List<Invatation> leafList = new LinkedList<Invatation>();
                for(ProgramInvitation leafInvitation:Leafresult){
                    LeafInvatation two = new LeafInvatation();
                    two.setNext(null); //叶子节点没有后继
                    two.setInvitation(leafInvitation);
                    String twoName = userService.getById(leafInvitation.getUId()).getName();
                    User twoUser = new User();
                    twoUser.setName(twoName);
                    two.setUser(twoUser);
                    if(leafInvitation.getPreNode()==programInvitation.getId()){
                        leafList.add(two);
                    }
                }
                one.setNext(leafList);
                stepList.add(one);
            }
            MainInvatation mainInvatation = new MainInvatation(null, null);
            mainInvatation.setNext(stepList);
            mainInvatation.setProgram(mainProgram);
            System.out.println(mainProgram.toString());
            String mainName = userService.getById(mainProgram.getOperatorId()).getName();
            User mainUser = new User();
            mainUser.setName(mainName);
            mainInvatation.setUser(mainUser);
            return Msg.success().add("data", mainInvatation);
        }
        else {
            return Msg.fail().add("error", "参数错误");
        }
    }

    @GetMapping("getDings")
    @ApiOperation("获取点赞指定id主贴的人员")
    public Msg getDings(@RequestParam("pid") Integer pid){
        if(pid==null){
            return Msg.fail().add("error", "参数错误");
        }
        QueryWrapper<ProgramInvitation> wrapper = new QueryWrapper<ProgramInvitation>();
        wrapper.eq("p_id", pid);
        wrapper.eq("invitation_content", "ding");
        List<ProgramInvitation> result = invitationService.list(wrapper);
        if(result!=null){
            return Msg.success().add("data", result);
        }else{
            return Msg.fail().add("error", "网络问题，稍后重试");
        }
    }
}

