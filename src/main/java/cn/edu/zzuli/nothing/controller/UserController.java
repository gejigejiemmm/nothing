package cn.edu.zzuli.nothing.controller;


import cn.edu.zzuli.nothing.bean.Msg;
import cn.edu.zzuli.nothing.bean.User;
import cn.edu.zzuli.nothing.service.UserService;
import cn.edu.zzuli.nothing.utils.BaseUtils;
import cn.edu.zzuli.nothing.utils.RedisUtil;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author geji
 * @since 2020-05-02
 */
@RestController
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    RedisUtil redisUtil;

//    @PostMapping("/user")
//    @ApiOperation("添加用户,id，创建时间和修改时间，deleted,role都不要传！")
//    public Msg add(@RequestBody @Valid User user) {
//        userService.save(user);
//        return Msg.success().add("user", user);
//    }

    @DeleteMapping("/user/{id}")
    @ApiOperation("查询用户,/user/2")
    @RequiresRoles("principal")
    public Msg delete(@PathVariable Integer id) {
        boolean res = userService.removeById(id);
        if (!res) {
            return Msg.fail().add("error","怎么出错了！？");
        }
        return Msg.success();
    }

    @PutMapping("/user")
    @ApiOperation("修改用户,json形式")
    public Msg delete(@RequestBody JSONObject json) {
        userService.updateByInfo(json);
        return Msg.success();
    }

    @GetMapping("/user/{id}")
    @ApiOperation("查询用户")
    public Msg get(@PathVariable @Valid @NotNull Integer id) {
        User user = userService.getById(id)
                    .setPassword("")
                    .setOpenId("");
        if (user == null) {
            return Msg.fail().add("error","没有该用户啊？你怎么回事？");
        }
        return Msg.success().add("user", user);
    }

    @PostMapping("/user/login")
    @ApiOperation("用户登录,{\"name\":\"xx\",\"password\":\"xx\"}")
    public Msg userLogin(@RequestBody JSONObject jsonObject){
        String name = jsonObject.getString("name");
        String password = jsonObject.getString("password");
        if(name ==null||password==null){
            return Msg.fail().add("msg", "用户名或者密码为null");
        }
        //获取到当前对象
        Subject subject = SecurityUtils.getSubject();

        //将用户名和密码封装进去
        UsernamePasswordToken token = new UsernamePasswordToken(name, password);

        //执行登陆方法
        subject.login(token);

        return Msg.success().add("user", SecurityUtils.getSubject().getPrincipal());

    }

    @GetMapping("/user/list")
    @ApiOperation("获取全部成员信息")
    public Msg getUserList() {
        List<User> users = userService.list();
        users.forEach(user -> {
            user.setDeleted(null);
            user.setCreateTime(null);
            user.setUpdateTime(null);
            user.setPassword(null);
            user.setOpenId(null);
        });
        return Msg.success().add("users",users);
    }

    @GetMapping("/user/info/{id}")
    @ApiOperation("用来获取该用户是否完善信息")
    public Msg jugeInfoIsPerfect(@PathVariable("id") Integer id) {
        boolean haveInfo = (Boolean) redisUtil.get(BaseUtils.getUserKey(id, "haveInfo"));
        return Msg.success().add("boolean",haveInfo);
    }

    @PostMapping("/user/excel")
    @ApiOperation("将用户信息导出到excel中，管理员才能够使用")
    @RequiresRoles("principal")
    public Msg excel() {
        return Msg.success().add("url",userService.userInfoToExcel());
    }
}

