package cn.edu.zzuli.nothing.controller;

import cn.edu.zzuli.nothing.bean.Msg;
import cn.edu.zzuli.nothing.utils.RedisUtil;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@RestController
@RequestMapping("/notice")
public class NoticeBoardController {

    @Autowired
    RedisUtil redisUtil;

    @PostMapping("/go")
    @RequiresRoles("principal")
    @ApiOperation("公告栏")
    public Msg addData(@RequestBody JSONObject req) {
        //这个接口不会长久使用，就是为了避免小程序带有社交标签，能够成功提交作品
        //在提交后记得实现之前的留言评论功能。halo
        String content = req.getString("content");
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String time = LocalDateTime.now().format(df);
        try {
            if (content != null){
                redisUtil.lSet("notice", content);
                redisUtil.lSet("notice", time);
            }
        } catch (Exception e) {
            return Msg.fail().add("error", e.getMessage());
        }
        return Msg.success().add("content", content)
                .add("time", time);
    }

    @GetMapping("/go")
    @ApiOperation("获取全部公告栏")
    public Msg get() {
        return Msg.success().add("noticeBoard",redisUtil.lGet("notice", 0, -1));
    }

}
