package cn.edu.zzuli.nothing.controller;


import cn.edu.zzuli.nothing.bean.Item;
import cn.edu.zzuli.nothing.bean.Msg;
import cn.edu.zzuli.nothing.service.ItemService;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author geji
 * @since 2020-05-22
 */
@RestController
@RequestMapping("/item")
public class ItemController {

    @Autowired
    ItemService itemService;

    //geji
    //不要问我为什么用 go，因为 golang永不为奴！
    @PostMapping("/go")
    @ApiOperation("添加一条项目记录")
    @RequiresRoles("principal")
    public Msg add(@RequestBody @Valid Item item) {
        System.out.println(item);
        if (itemService.add(item)) {
            return Msg.success();
        }
        return Msg.fail();
    }

    @GetMapping("/go/{id}")
    @ApiOperation("id为0，查询所有，其余为正常查询")
    //别问为什么这么设计？脑子抽了
    public Msg get(@PathVariable(value = "id") Integer itemId) {
        return itemService.get(itemId);
    }

    @DeleteMapping("/go/{id}")
    @ApiOperation("根据id删除")
    @RequiresRoles("principal")
    public Msg delete(@PathVariable(value = "id") Integer itemId) {
        if (itemService.removeById(itemId)) {
            //详情的话感觉删不删无所谓。因为所有详情的查找都是根据itemid来找的。
            // 而且我们的删除都是逻辑意义上的删除，所以这里偷个懒？
            return Msg.success();
        }

        return Msg.fail().add("error","好像哪里出错了");
    }

    @PutMapping("/go")
    @RequiresRoles("principal")
    public Msg update(@RequestBody JSONObject json) {
        return itemService.update(json);
    }

}

