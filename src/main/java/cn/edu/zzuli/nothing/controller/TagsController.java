package cn.edu.zzuli.nothing.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author geji
 * @since 2020-06-11
 */
@RestController
@RequestMapping("/tags")
public class TagsController {

}

