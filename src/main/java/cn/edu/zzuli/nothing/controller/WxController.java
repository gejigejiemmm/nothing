package cn.edu.zzuli.nothing.controller;

import cn.edu.zzuli.nothing.bean.Msg;
import cn.edu.zzuli.nothing.bean.User;
import cn.edu.zzuli.nothing.service.WxService;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

@RestController
@RequestMapping("/wx")
public class WxController {

    @Autowired
    WxService wxService;

    @PostMapping("/login")
    @ApiOperation("微信登录，code是小程序的临时凭证")
    private Msg login(@RequestBody JSONObject req) {
        String code = req.getString("code");
        String encryptedData = req.getString("encryptedData");
        String iv = req.getString("iv");

        if (code != null && iv != null && encryptedData != null) {
            //进行登录逻辑
            return wxService.login(code, iv, encryptedData);
        }

        return Msg.fail();
    }

}
