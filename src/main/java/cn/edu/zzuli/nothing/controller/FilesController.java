package cn.edu.zzuli.nothing.controller;

import cn.edu.zzuli.nothing.bean.Msg;
import cn.edu.zzuli.nothing.service.FileService;
import cn.edu.zzuli.nothing.utils.AliOssUtils;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.Size;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@RestController
@RequestMapping("/file")
public class FilesController {

    @Autowired
    FileService fileService;

    @Autowired
    AliOssUtils aliOssUtils;

    @GetMapping("/url/{name}")
    @ApiOperation("获取阿里云oss上的文件，传入文件名即可")
    public Msg getFileUrl(@PathVariable String name) {

        URL url = aliOssUtils.getUrl(name);
        if (url == null) {
            return Msg.fail().add("error","好像哪里出错了");
        }
        return Msg.success().add("getUrl",url);
    }

    @GetMapping("/put/{name}")
    @ApiOperation("获取上传文件的url，拿到url发送put请求，带上文件即可，传入name即上传文件名")
    public Msg getPutFileUrl(@PathVariable String name) {
        URL putUrl = aliOssUtils.getPutUrl(name);
        URL getUrl = aliOssUtils.getUrl(name);
        return Msg.success().add("putUrl",putUrl).add("getUrl",getUrl);
    }

    @PostMapping("/upload/{folder}/{name}")
    @ApiOperation("无法采用直传的替代接口")
    public Msg uploadFile(MultipartFile file, @PathVariable("folder") String folder,
                          @PathVariable("name") String name) {
        //继续垃圾小程序...给的名字还是乱码...
        if (file.getSize() >= 1024 * 1024 * 5) {
            return Msg.fail().add("error","文件不能超过5M");
        }

        return fileService.uploadFile(file, folder, name);
    }
}
