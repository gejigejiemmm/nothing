package cn.edu.zzuli.nothing.config.realm;

import cn.edu.zzuli.nothing.bean.*;
import cn.edu.zzuli.nothing.mapper.PermissionMapper;
import cn.edu.zzuli.nothing.mapper.RoleMapper;
import cn.edu.zzuli.nothing.mapper.UserMapper;
import cn.edu.zzuli.nothing.utils.BaseUtils;
import cn.edu.zzuli.nothing.utils.RedisUtil;
import cn.edu.zzuli.nothing.utils.TokenUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class WxTokenRealm extends AuthorizingRealm {

    @Autowired
    UserMapper userMapper;

    @Autowired
    RoleMapper roleMapper;

    @Autowired
    PermissionMapper permissionMapper;

    @Autowired
    RedisUtil redisUtil;

    /**
     * 如果不重写这个方法会一直 token校验失败
     * @param token
     * @return
     */
    @Override
    public boolean supports(AuthenticationToken token) {
        return token instanceof JWTToken;
    }

    //执行授权逻辑
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        //当前登录的对象的id
        String userId = TokenUtil.getAccount(principalCollection.toString());
        SimpleAuthorizationInfo redisInfo =
                (SimpleAuthorizationInfo) redisUtil.get("user:" + userId + ":author");
        if (redisInfo != null){
            return redisInfo;
        }

        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();

        //根据id获取对象role
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.select(User::getRole).eq(User::getId,Integer.parseInt(userId));
        User user = userMapper.selectOne(queryWrapper);

        //获取当前对象角色，这次我们时一人一个角色。一人多角色好像没必要(主要是懒的改表了)
        Role curRole = roleMapper.selectById(user.getRole());
        //根据当前对象角色获取对应的资源权限
        Set<String> roles = new HashSet<>();
        roles.add(curRole.getRoleName());
        info.setRoles(roles);

        List<Integer> integers = permissionMapper.permissionIdByRole(curRole);

        //获取角色所拥有的资源权限
        LambdaQueryWrapper<Permission> permissionLambdaQueryWrapper = new LambdaQueryWrapper<>();
        permissionLambdaQueryWrapper.select(Permission::getPerssionValue)
                .in(Permission::getId, integers);
        List<Permission> permissions = permissionMapper.selectList(permissionLambdaQueryWrapper);
        //添加权限
        permissions.forEach(permission -> {
            info.addStringPermission(permission.getPerssionValue());
        });
        redisUtil.set("user:"+userId+":author", info);
        return info;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authToken) throws AuthenticationException {

        //因为每次验证token的时候都会来执行一次认证方法
        //但我们在生成token的时候就已经认证过了，我觉得在这里就没有必要了。
        //如果害怕别人伪造token的话，可以再次判断一次。
        if (!(authToken instanceof JWTToken)) {
            return null;
        }

        String token = (String) authToken.getCredentials();
        String user = TokenUtil.getAccount(token);

        if (user != null) {
            return new SimpleAuthenticationInfo(token, token, this.getClass().getSimpleName());
        }

        return null;
    }
}
