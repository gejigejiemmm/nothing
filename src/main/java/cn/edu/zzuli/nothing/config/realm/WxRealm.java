package cn.edu.zzuli.nothing.config.realm;

import cn.edu.zzuli.nothing.bean.*;
import cn.edu.zzuli.nothing.mapper.PermissionMapper;
import cn.edu.zzuli.nothing.mapper.RoleMapper;
import cn.edu.zzuli.nothing.mapper.UserMapper;
import cn.edu.zzuli.nothing.utils.BaseUtils;
import cn.edu.zzuli.nothing.utils.RedisUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class WxRealm extends AuthorizingRealm {

    @Autowired
    UserMapper userMapper;

    @Autowired
    RoleMapper roleMapper;

    @Autowired
    PermissionMapper permissionMapper;

    @Autowired
    RedisUtil redisUtil;



    //执行授权逻辑
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {

        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();

        //当前登录的对象。
        Subject subject = SecurityUtils.getSubject();
        User customer = (User)subject.getPrincipal();

        //获取当前对象角色，这次我们时一人一个角色。一人多角色好像没必要(主要是懒的改表了)
        Role curRole = roleMapper.selectById(customer.getRole());
        //根据当前对象角色获取对应的资源权限
        Set<String> roles = new HashSet<>();
        roles.add(curRole.getRoleName());
        info.setRoles(roles);

        List<Integer> integers = permissionMapper.permissionIdByRole(curRole);

        //获取角色所拥有的资源权限
        LambdaQueryWrapper<Permission> permissionLambdaQueryWrapper = new LambdaQueryWrapper<>();
        permissionLambdaQueryWrapper.select(Permission::getPerssionValue)
                .in(Permission::getId, integers);
        List<Permission> permissions = permissionMapper.selectList(permissionLambdaQueryWrapper);
        //添加权限
        permissions.forEach(permission -> {
            info.addStringPermission(permission.getPerssionValue());
        });

        return info;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        JSONObject userinfo = (JSONObject) token.getPrincipal();
        String openId = userinfo.getString("openId");

        //判断是否是微信登录
        if (token instanceof WxOpenIdToken) {
            //根据openid查询是否有已绑定的userid,有就时已绑定
            LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.eq(User::getOpenId, openId);
            User user = userMapper.selectOne(queryWrapper);
            if (openId == null) {
                return null;
            }

            //说明，还没有使用过系统，新增加一条数据，与 openId进行绑定
            if (user == null) {
                System.out.println(openId);
                user = new User().setOpenId(openId)
                        .setAvater(userinfo.getString("avatarUrl"));
                userMapper.insert(user);
                redisUtil.set(BaseUtils.getUserKey(user.getId(),"haveInfo"), false);
                redisUtil.set(BaseUtils.getKey(User.class, user.getId() + ":times"), 0);
                redisUtil.set(BaseUtils.getKey(User.class, user.getId() + ":email"), true);
            }
            user.setOpenId("");
            user.setPassword("");

            //为了防止用户修改头像，每次判断一下。如果不一样要去修改。
            String avatarUrl = userinfo.getString("avatarUrl");
            if (!avatarUrl.equals(user.getAvater())) {
                User update = new User();
                update.setAvater(avatarUrl)
                .setId(user.getId());
                //只修改头像
                userMapper.update(update,null);
            }

            return new SimpleAuthenticationInfo(user, "ok", this.getClass().getSimpleName());
        }else {
            return null;
        }
    }
}
