package cn.edu.zzuli.nothing.config.realm;

import cn.edu.zzuli.nothing.bean.User;
import cn.edu.zzuli.nothing.service.UserService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;

public class UserRealm extends AuthorizingRealm {

    @Autowired
    private UserService userService;

    //执行授权逻辑
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {

        Subject subject = SecurityUtils.getSubject();
        User customer = (User)subject.getPrincipal();
        System.out.println(customer.getName()+"开始执行授权逻辑！");
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        info.addStringPermission("user:*");
        return info;
    }

    //执行认证逻辑，执行login()方法以后，会进入到这里，进行用户名密码的比对
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {

        //执行认证逻辑
        UsernamePasswordToken token = (UsernamePasswordToken) authenticationToken;

        //获取条件查询器
        QueryWrapper<User> wrapper = new QueryWrapper<>();

        //获取用户密码
        wrapper.eq("name", token.getUsername());
        User user = userService.getOne(wrapper);

        if(user==null){
            throw new UnknownAccountException();
        }
        else{
            return new SimpleAuthenticationInfo(user, user.getPassword(), "userRealm");
        }
    }
}
