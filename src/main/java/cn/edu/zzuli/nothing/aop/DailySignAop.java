package cn.edu.zzuli.nothing.aop;

import cn.edu.zzuli.nothing.bean.Msg;
import cn.edu.zzuli.nothing.bean.User;
import cn.edu.zzuli.nothing.utils.BaseUtils;
import cn.edu.zzuli.nothing.utils.RedisUtil;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.time.Duration;
import java.time.LocalDateTime;

@Aspect
@Component
public class DailySignAop {

    @Autowired
    RedisUtil redisUtil;
    /**
     * 定义切入点，切入点为com.example.demo.aop.AopController中的所有函数
     *通过@Pointcut注解声明频繁使用的切点表达式
     */
    @Around("execution(public * cn.edu.zzuli.nothing.service.aboutSign.DailySign.sign(..)))")
    public Object beforeSign(ProceedingJoinPoint point) {
        //首先获取 redis中记录的状态标志
//        redisUtil.get(BaseUtils.getKey(""))
        Object result = null;
        try {
            Object[] args = point.getArgs();
            //这个就是 用户的 id
            String submit = (String) redisUtil.get(BaseUtils.getKey(User.class,args[0]+":submit"));

            if (submit != null) {
                //只要 submit还在，那就是已经签到过了。现在我们要判断的是重复提交，还是签退这两种情况
                LocalDateTime inTime = LocalDateTime.parse(submit);
                LocalDateTime now = LocalDateTime.now();

                //计算相差多少分钟
                Duration duration = Duration.between(inTime, now);
                long minutes = duration.toMinutes();

                //如果小于半小时，视为重复提交。也就是说，签退时间最多半小时。
                if (minutes < 2) {
                    return Msg.fail().setMsg("请不要再半小时内重复提交了:)");
                }
            }

            result = point.proceed(args);
        }catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return result;
    }
}
