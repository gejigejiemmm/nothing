package cn.edu.zzuli.nothing;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableScheduling;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableScheduling//添加定时任务的注解
@EnableAspectJAutoProxy
public class NothingApplication {

    public static void main(String[] args) {
        SpringApplication.run(NothingApplication.class, args);
    }

}
