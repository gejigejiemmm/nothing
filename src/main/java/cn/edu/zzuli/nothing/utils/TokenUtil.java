package cn.edu.zzuli.nothing.utils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.DecodedJWT;
import lombok.extern.slf4j.Slf4j;

import java.util.Date;

/**
 * token工具类（生成、验证）
 */
@Slf4j
public class TokenUtil {

    public static final long EXPIRE_TIME= 24 * 3600 * 1000;//token到期时间24小时，毫秒为单位
    public static final long REFRESH_EXPIRE_TIME=168 * 3600;//RefreshToken到期时间为168小时，也就是一周秒为单位
    private static final String TOKEN_SECRET="nothing**333j??";  //密钥盐,为了加盐，内容随意

    /**
     * 生成token
     */
    public static String sign(String account, Long currentTime){

        String token=null;
        try {
            Date expireAt=new Date(currentTime+EXPIRE_TIME);
//            System.out.println(expireAt);
            token = JWT.create()
                    .withIssuer("auth0")//发行人
                    .withClaim("account",account)//存放数据
                    .withClaim("currentTime",currentTime)
                    .withExpiresAt(expireAt)//过期时间
                    .sign(Algorithm.HMAC256(TOKEN_SECRET));
            log.info("用户" + account +":"+token);
        } catch (IllegalArgumentException |JWTCreationException je) {

        }
        return token;
    }


    /**
     * token验证
     */
    public static Boolean verify(String token) throws Exception {

        JWTVerifier jwtVerifier = JWT.require(Algorithm.HMAC256(TOKEN_SECRET)).withIssuer("auth0").build();//创建token验证器
        DecodedJWT decodedJWT = jwtVerifier.verify(token);
//        System.out.println("认证通过：");
//        System.out.println("account: " + decodedJWT.getClaim("account").asString());
//        System.out.println("过期时间：      " + decodedJWT.getExpiresAt());
        return true;
    }



    public static String getAccount(String token){
        try{
            DecodedJWT decodedJWT = JWT.decode(token);
//            System.out.println(decodedJWT.getClaim("account").getClass());
            return decodedJWT.getClaim("account").asString();

        }catch (JWTCreationException ce){
            log.error("token 创建异常" + ce.getMessage());
            return null;
        }catch (JWTDecodeException de) {
            log.error("token 解析异常" + de.getMessage());
            return null;
        }
    }

    public static Long getCurrentTime(String token){
        try{
            DecodedJWT decodedJWT = JWT.decode(token);
            return decodedJWT.getClaim("currentTime").asLong();

        }catch (JWTCreationException ce){
            log.error("token 创建异常" + ce.getMessage());
            return null;
        }catch (JWTDecodeException de) {
            log.error("token 解析异常" + de.getMessage());
            return null;
        }
    }

}
