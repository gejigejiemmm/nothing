package cn.edu.zzuli.nothing.utils;

import io.swagger.models.auth.In;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.formula.functions.T;

import java.lang.reflect.Field;

@Slf4j
public class BaseUtils {

    public static String getUserKey(Integer uId, String msg) {
        return "user:"+uId+":"+msg;
    }

    public static String getKey(String name,String msg) {
        return name+":"+msg;
    }
    //这个其实不一定会用到，看你想不想自己拼接了
    public static String getKey(Class clazz,String msg) {
        return clazz.getSimpleName().toLowerCase()+":"+msg;
    }

    public static String getKey(Object obj,String msg) {
        Class clazz = obj.getClass();
        Integer id = 0;
        try {
            Field idFiled = clazz.getDeclaredField("id");
            idFiled.setAccessible(true);
            id = (Integer) idFiled.get(obj);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            log.info("没有 id 这个字段");
        }
        return clazz.getSimpleName().toLowerCase()+":"+id+":"+msg;
    }

}
