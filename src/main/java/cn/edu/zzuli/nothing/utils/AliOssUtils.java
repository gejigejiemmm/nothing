package cn.edu.zzuli.nothing.utils;


import com.aliyun.oss.*;
import com.aliyun.oss.common.auth.CredentialsProvider;
import com.aliyun.oss.common.auth.DefaultCredentialProvider;
import com.aliyun.oss.model.GeneratePresignedUrlRequest;
import com.aliyun.oss.model.OSSObject;
import com.aliyun.oss.model.PutObjectResult;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.auth.sts.AssumeRoleRequest;
import com.aliyuncs.auth.sts.AssumeRoleResponse;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 自己写的一些用来
 * 操作 oss 的工具类
 * @author geji
 */
@Data
@Component
public class AliOssUtils {

    // Endpoint以杭州为例，其它Region请按实际情况填写。
    @Value("${aliyun.oss.EndPoint}")
    String endpoint;
    // 阿里云主账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM账号进行API访问或日常运维，请登录 https://ram.console.aliyun.com 创建RAM账号。
    @Value("${aliyun.oss.AccessKeyID}")
    String accessKeyId;

    @Value("${aliyun.oss.AccessKeySecret}")
    String accessKeySecret;

    @Value("${aliyun.oss.BucketName}")
    String bucketName;

    @Value("${aliyun.oss.RoleArn}")
    String roleArn;

    @Value("${aliyun.oss.roleSessionName}")
    String roleSessionName = "suzy";

    //默认时长六分钟
    int expire = 360 * 1000;

    /**
     * 获取 sts 授权
     * @return url
     */
    public URL getUrl(String objectName) {
        //采取默认的过期时间
        return this.getUrl(objectName,expire);
    }

    //自定义过期时间
    public URL getUrl(String objectName, int expire) {
        try {
        // 创建OSSClient实例。
        OSS ossClient = getStsClient();

        // 设置URL过期时间
        Date expiration = new Date(new Date().getTime() + expire);
        // 生成以GET方法访问的签名URL，访客可以直接通过浏览器访问相关内容。
        URL url = ossClient.generatePresignedUrl(bucketName, objectName, expiration);
        // 关闭OSSClient。
        ossClient.shutdown();
        return url;
        }catch (Exception e){
            return null;
        }

    }


    /**
     * 获取 上传文件的 put url
     * @param objectName
     * @return put url
     */
    public URL getPutUrl(String objectName) {
        return this.putFile(objectName,null);
    }

    /**
     * 传入名字，直接上传，不返回URL
     */
    public URL putFile(String objectName, File f) {
        OSS ossClient = getStsClient();

        Date expiration = new Date(new Date().getTime() + expire);
        //因为小程序的垃圾被迫改为服务器上传，直传失败，牛的，无力吐槽！为什么 wx.uploadFile不支持 PUT请求？？
        GeneratePresignedUrlRequest request = new GeneratePresignedUrlRequest(bucketName, objectName, HttpMethod.PUT);
        // 设置过期时间。
        request.setExpiration(expiration);
        // 设置Content-Type。这两个参数前端要带上
        request.setContentType("application/octet-stream");
        // 添加用户自定义元信息。这两个参数前端要带上
        request.addUserMetadata("author", "aliy");
        // 生成签名URL
        URL signedUrl = ossClient.generatePresignedUrl(request);

        // 使用签名URL发送请求。
        if (f != null){
            InputStream fin = null;
            try {
                fin = new FileInputStream(f);
            } catch (FileNotFoundException e) {
                System.out.println("找不到文件");
            }
            // 添加PutObject请求头。
            Map<String, String> customHeaders = new HashMap<String, String>();
            customHeaders.put("Content-Type", "application/octet-stream");
            customHeaders.put("x-oss-meta-author", "aliy");

            PutObjectResult result = ossClient.putObject(signedUrl, fin, f.length(), customHeaders);
            return getUrl(objectName);
        }

        // 关闭OSSClient。
        ossClient.shutdown();
        return signedUrl;
    }

    /**
     * 不采用直传，由后端上传到阿里云OSS
     * @param file
     */
    public boolean uploadFle(MultipartFile file, String ossFolder, String name) {
        // 创建OSSClient实例。
        OSS ossClient = getStsClient();
        try {
            InputStream inputStream = file.getInputStream();
            String url = "";
            //如果需要分目录来上传，需要进行一下拼接
            if (ossFolder != null) {
                url += ossFolder + "/" + name;
            }else {
                url = name;
            }

            //判断文件是否存在
            if (!ossClient.doesObjectExist(bucketName, url)) {
                ossClient.putObject(bucketName, url, inputStream);
                return true;
            }
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        } finally {
            System.out.println("shutdown");
            ossClient.shutdown();
        }

    }

    /**
     * 下载文件
     * @param objectName
     */
    public void getFile(String objectName) {
        OSS ossClient =getStsClient();
        GeneratePresignedUrlRequest request = new GeneratePresignedUrlRequest(bucketName, objectName, HttpMethod.GET);
        Date expiration = new Date(new Date().getTime() + expire);
        // 设置过期时间。
        request.setExpiration(expiration);
        // 生成签名URL（HTTP GET请求）。
        URL signedUrl = ossClient .generatePresignedUrl(request);
        System.out.println("signed url for getObject: " + signedUrl);

        // 使用签名URL发送请求。
        Map<String, String> customHeaders = new HashMap<String, String>();
        // 添加GetObject请求头。
        customHeaders.put("Range", "bytes=100-1000");
        OSSObject object = ossClient.getObject(signedUrl,customHeaders);

        // 关闭OSSClient。
        ossClient.shutdown();
    }

    /**
     * 获取 临时令牌 sts
     * @return 返回 ossClient对象
     */
    public OSSClient getStsClient() {

        String policy = "{\n" +
                "    \"Version\": \"1\",\n" +
                "    \"Statement\": [\n" +
                "        {\n" +
                "            \"Effect\": \"Allow\",\n" +
                "            \"Action\": [\n" +
                "                \"oss:Put*\",\n" +
                "                \"oss:Get*\"\n" +
                "            ],\n" +
                "            \"Resource\": [\n" +
                "                \"acs:oss:*:*:go-giligili/*\"\n" +
                "            ]\n" +
                "        }\n" +
                "    ]\n" +
                "}";
        try {
            // 添加endpoint（直接使用STS endpoint，前两个参数留空，无需添加region ID）
            DefaultProfile.addEndpoint("", "", "Sts", "sts.aliyuncs.com");
            // 构造default profile（参数留空，无需添加region ID）
            IClientProfile profile = DefaultProfile.getProfile("", accessKeyId, accessKeySecret);
            // 用profile构造client
            DefaultAcsClient client = new DefaultAcsClient(profile);
            final AssumeRoleRequest request = new AssumeRoleRequest();
            request.setMethod(MethodType.POST);
            request.setRoleArn(roleArn);
            request.setRoleSessionName(roleSessionName);
            request.setPolicy(policy); // 若policy为空，则用户将获得该角色下所有权限
            request.setDurationSeconds(1000L); // 设置凭证有效时间
            request.setConnectTimeout(10000);
            request.setReadTimeout(10000);
            final AssumeRoleResponse response = client.getAcsResponse(request);
//            System.out.println("Expiration: " + response.getCredentials().getExpiration());
//            System.out.println("Access Key Id: " + response.getCredentials().getAccessKeyId());
//            System.out.println("Access Key Secret: " + response.getCredentials().getAccessKeySecret());
//            System.out.println("Security Token: " + response.getCredentials().getSecurityToken());
//            System.out.println("RequestId: " + response.getRequestId());

            String ossAccessKeyId = response.getCredentials().getAccessKeyId();
            String ossAccessKeySecret = response.getCredentials().getAccessKeySecret();
            String ossSecurityToken = response.getCredentials().getSecurityToken();

            return new OSSClient(endpoint,
                    (CredentialsProvider)(new DefaultCredentialProvider(ossAccessKeyId, ossAccessKeySecret, ossSecurityToken)),
                    (ClientConfiguration)null);

        } catch (ClientException e) {
            System.out.println("Failed：");
            System.out.println("Error code: " + e.getErrCode());
            System.out.println("Error message: " + e.getErrMsg());
            System.out.println("RequestId: " + e.getRequestId());

            return null;
        }
    }




}
