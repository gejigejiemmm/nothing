package cn.edu.zzuli.nothing.mapper;

import cn.edu.zzuli.nothing.bean.Tags;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author geji
 * @since 2020-06-11
 */
public interface TagsMapper extends BaseMapper<Tags> {

}
