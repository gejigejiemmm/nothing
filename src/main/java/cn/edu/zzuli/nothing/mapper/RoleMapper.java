package cn.edu.zzuli.nothing.mapper;

import cn.edu.zzuli.nothing.bean.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author geji
 * @since 2020-05-02
 */
public interface RoleMapper extends BaseMapper<Role> {

}
