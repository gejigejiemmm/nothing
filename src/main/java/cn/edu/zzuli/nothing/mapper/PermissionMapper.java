package cn.edu.zzuli.nothing.mapper;

import cn.edu.zzuli.nothing.bean.Permission;
import cn.edu.zzuli.nothing.bean.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author geji
 * @since 2020-05-02
 */
public interface PermissionMapper extends BaseMapper<Permission> {

    //先从第三表拿出来角色对应的权限Id
    @Select("SELECT p_id from oa_role_permission WHERE r_id = #{id}")
    List<Integer> permissionIdByRole(Role role);
}
